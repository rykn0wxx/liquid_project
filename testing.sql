USE [Liquid]
GO

SELECT
  uv.ticket_number,
  f.id

FROM
  dbo.uvTmpFactTicket AS uv LEFT JOIN dbo.fact_tickets AS f
  ON uv.dim_project_id = f.dim_project_id AND
  uv.ticket_number = f.ticket_number AND
  uv.ticket_created_at = f.ticket_created_at AND
  uv.dim_ticket_type_id = f.dim_ticket_type_id

WHERE
  f.id Is NULL
