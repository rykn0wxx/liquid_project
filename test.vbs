Option Explicit

Dim respMessage

Public Sub MainApp()
  respMessage = InputBox("Test only", "Test Title")
  If respMessage = vbNullString then
    WScript.Echo "No response"
  Else
    WScript.Echo respMessage
  End If
End Sub

MainApp

WScript.Quit
