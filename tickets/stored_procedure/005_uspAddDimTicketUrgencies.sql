USE [Liquid]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	  ariel.c.andrade
-- Create date: 11-04-2019
-- Description:	Add new dimension
-- =============================================

IF OBJECT_ID ( 'dbo.uspAddDimTicketUrgencies', 'P' ) IS NOT NULL
  DROP PROCEDURE [dbo].[uspAddDimTicketUrgencies]
GO

CREATE PROCEDURE [dbo].[uspAddDimTicketUrgencies]
AS
  SET NOCOUNT ON;
  BEGIN
    INSERT INTO [dbo].[dim_ticket_urgencies] ([ticket_urgency])
    SELECT DISTINCT [tt].[dim_ticket_urgency_id]
    FROM [dbo].[tmp_fact_tickets] AS tt
    LEFT JOIN [dbo].[dim_ticket_urgencies] AS dp ON [tt].[dim_ticket_urgency_id] = [dp].[ticket_urgency]
    WHERE (([dp].[id] Is NULL) AND ([tt].[dim_ticket_urgency_id] Is NOT NULL));
  END
GO
