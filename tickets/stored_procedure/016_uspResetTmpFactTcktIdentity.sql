USE [Liquid]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	  ariel.c.andrade
-- Create date: 15-04-2019
-- =============================================

IF OBJECT_ID ( 'dbo.uspResetTmpFactTcktIdentity', 'P' ) IS NOT NULL
  DROP PROCEDURE [dbo].[uspResetTmpFactTcktIdentity]
GO

CREATE PROCEDURE [dbo].[uspResetTmpFactTcktIdentity]
AS
  SET NOCOUNT ON;
  BEGIN

    DBCC CHECKIDENT('tmp_fact_tickets', RESEED, 0)

  END
GO
