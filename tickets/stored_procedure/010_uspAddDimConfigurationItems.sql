USE [Liquid]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	  ariel.c.andrade
-- Create date: 11-04-2019
-- Description:	Add new dimension
-- =============================================

IF OBJECT_ID ( 'dbo.uspAddDimConfigurationItems', 'P' ) IS NOT NULL
  DROP PROCEDURE [dbo].[uspAddDimConfigurationItems]
GO

CREATE PROCEDURE [dbo].[uspAddDimConfigurationItems]
AS
  SET NOCOUNT ON;
  BEGIN
    INSERT INTO [dbo].[dim_configuration_items] ([configuration_item])
    SELECT DISTINCT [tt].[dim_configuration_item_id]
    FROM [dbo].[tmp_fact_tickets] AS tt
    LEFT JOIN [dbo].[dim_configuration_items] AS dp ON [tt].[dim_configuration_item_id] = [dp].[configuration_item]
    WHERE (([dp].[id] Is NULL) AND ([tt].[dim_configuration_item_id] Is NOT NULL));
  END
GO
