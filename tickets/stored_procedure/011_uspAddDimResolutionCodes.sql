USE [Liquid]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	  ariel.c.andrade
-- Create date: 11-04-2019
-- Description:	Add new dimension
-- =============================================

IF OBJECT_ID ( 'dbo.uspAddDimResolutionCodes', 'P' ) IS NOT NULL
  DROP PROCEDURE [dbo].[uspAddDimResolutionCodes]
GO

CREATE PROCEDURE [dbo].[uspAddDimResolutionCodes]
AS
  SET NOCOUNT ON;
  BEGIN
    INSERT INTO [dbo].[dim_resolution_codes] ([resolution_code])
    SELECT DISTINCT [tt].[dim_resolution_code_id]
    FROM [dbo].[tmp_fact_tickets] AS tt
    LEFT JOIN [dbo].[dim_resolution_codes] AS dp ON [tt].[dim_resolution_code_id] = [dp].[resolution_code]
    WHERE (([dp].[id] Is NULL) AND ([tt].[dim_resolution_code_id] Is NOT NULL));
  END
GO
