USE [Liquid]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	  ariel.c.andrade
-- Create date: 11-04-2019
-- Description:	Add new dimension
-- =============================================

IF OBJECT_ID ( 'dbo.uspAddDimTicketImpacts', 'P' ) IS NOT NULL
  DROP PROCEDURE [dbo].[uspAddDimTicketImpacts]
GO

CREATE PROCEDURE [dbo].[uspAddDimTicketImpacts]
AS
  SET NOCOUNT ON;
  BEGIN

    INSERT INTO [dbo].[dim_ticket_impacts] ([ticket_impact])
    SELECT DISTINCT [tt].[dim_ticket_impact_id]
    FROM [dbo].[tmp_fact_tickets] AS tt
    LEFT JOIN [dbo].[dim_ticket_impacts] AS dp ON [tt].[dim_ticket_impact_id] = [dp].[ticket_impact]
    WHERE (([dp].[id] Is NULL) AND ([tt].[dim_ticket_impact_id] Is NOT NULL));
  END
GO
