USE [Liquid]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	  ariel.c.andrade
-- Create date: 11-04-2019
-- Description:	Add new dimension
-- =============================================

IF OBJECT_ID ( 'dbo.uspAddDimTicketSubcategories', 'P' ) IS NOT NULL
  DROP PROCEDURE [dbo].[uspAddDimTicketSubcategories]
GO

CREATE PROCEDURE [dbo].[uspAddDimTicketSubcategories]
AS
  SET NOCOUNT ON;
  BEGIN
    INSERT INTO [dbo].[dim_ticket_subcategories] ([ticket_subcategory])
    SELECT DISTINCT [tt].[dim_ticket_subcategory_id]
    FROM [dbo].[tmp_fact_tickets] AS tt
    LEFT JOIN [dbo].[dim_ticket_subcategories] AS dp ON [tt].[dim_ticket_subcategory_id] = [dp].[ticket_subcategory]
    WHERE (([dp].[id] Is NULL) AND ([tt].[dim_ticket_subcategory_id] Is NOT NULL));
  END
GO
