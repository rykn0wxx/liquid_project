USE [Liquid]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	  ariel.c.andrade
-- Create date: 11-04-2019
-- Description:	Add new dimension
-- =============================================

IF OBJECT_ID ( 'dbo.uspAddDimTicketPriorities', 'P' ) IS NOT NULL
  DROP PROCEDURE [dbo].[uspAddDimTicketPriorities]
GO

CREATE PROCEDURE [dbo].[uspAddDimTicketPriorities]
AS
  SET NOCOUNT ON;
  BEGIN
    INSERT INTO [dbo].[dim_ticket_priorities] ([ticket_priority])
    SELECT DISTINCT [tt].[dim_ticket_priority_id]
    FROM [dbo].[tmp_fact_tickets] AS tt
    LEFT JOIN [dbo].[dim_ticket_priorities] AS dp ON [tt].[dim_ticket_priority_id] = [dp].[ticket_priority]
    WHERE (([dp].[id] Is NULL) AND ([tt].[dim_ticket_priority_id] Is NOT NULL));
  END
GO
