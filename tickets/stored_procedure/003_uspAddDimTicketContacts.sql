USE [Liquid]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	  ariel.c.andrade
-- Create date: 11-04-2019
-- Description:	Add new dimension
-- =============================================

IF OBJECT_ID ( 'dbo.uspAddDimTicketContacts', 'P' ) IS NOT NULL
  DROP PROCEDURE [dbo].[uspAddDimTicketContacts]
GO

CREATE PROCEDURE [dbo].[uspAddDimTicketContacts]
AS
  SET NOCOUNT ON;
  BEGIN
    INSERT INTO [dbo].[dim_ticket_contacts] ([ticket_contact])
    SELECT DISTINCT [tt].[dim_ticket_contact_id]
    FROM [dbo].[tmp_fact_tickets] AS tt
    LEFT JOIN [dbo].[dim_ticket_contacts] AS dp ON [tt].[dim_ticket_contact_id] = [dp].[ticket_contact]
    WHERE (([dp].[id] Is NULL) AND ([tt].[dim_ticket_contact_id] Is NOT NULL));
  END
GO
