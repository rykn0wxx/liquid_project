USE [Liquid]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	  ariel.c.andrade
-- Create date: 11-04-2019
-- Description:	Add new dimension
-- =============================================

IF OBJECT_ID ( 'dbo.uspAddDimTicketCategories', 'P' ) IS NOT NULL
  DROP PROCEDURE [dbo].[uspAddDimTicketCategories]
GO

CREATE PROCEDURE [dbo].[uspAddDimTicketCategories]
AS
  SET NOCOUNT ON;
  BEGIN
    INSERT INTO [dbo].[dim_ticket_categories] ([ticket_category])
    SELECT DISTINCT [tt].[dim_ticket_category_id]
    FROM [dbo].[tmp_fact_tickets] AS tt
    LEFT JOIN [dbo].[dim_ticket_categories] AS dp ON [tt].[dim_ticket_category_id] = [dp].[ticket_category]
    WHERE (([dp].[id] Is NULL) AND ([tt].[dim_ticket_category_id] Is NOT NULL));
  END
GO
