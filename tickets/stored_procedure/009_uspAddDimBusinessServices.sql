USE [Liquid]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	  ariel.c.andrade
-- Create date: 11-04-2019
-- Description:	Add new dimension
-- =============================================

IF OBJECT_ID ( 'dbo.uspAddDimBusinessServices', 'P' ) IS NOT NULL
  DROP PROCEDURE [dbo].[uspAddDimBusinessServices]
GO

CREATE PROCEDURE [dbo].[uspAddDimBusinessServices]
AS
  SET NOCOUNT ON;
  BEGIN
    INSERT INTO [dbo].[dim_business_services] ([business_service])
    SELECT DISTINCT [tt].[dim_business_service_id]
    FROM [dbo].[tmp_fact_tickets] AS tt
    LEFT JOIN [dbo].[dim_business_services] AS dp ON [tt].[dim_business_service_id] = [dp].[business_service]
    WHERE (([dp].[id] Is NULL) AND ([tt].[dim_business_service_id] Is NOT NULL));
  END
GO
