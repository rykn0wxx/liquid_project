USE [Liquid]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	  ariel.c.andrade
-- Create date: 11-04-2019
-- Description:	Add new dimension
-- =============================================

IF OBJECT_ID ( 'dbo.uspAddDimTicketStates', 'P' ) IS NOT NULL
  DROP PROCEDURE [dbo].[uspAddDimTicketStates]
GO

CREATE PROCEDURE [dbo].[uspAddDimTicketStates]
AS
  SET NOCOUNT ON;
  BEGIN
    INSERT INTO [dbo].[dim_ticket_states] ([ticket_state])
    SELECT DISTINCT [tt].[dim_ticket_state_id]
    FROM [dbo].[tmp_fact_tickets] AS tt
    LEFT JOIN [dbo].[dim_ticket_states] AS dp ON [tt].[dim_ticket_state_id] = [dp].[ticket_state]
    WHERE (([dp].[id] Is NULL) AND ([tt].[dim_ticket_state_id] Is NOT NULL));
  END
GO
