USE [Liquid]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	  ariel.c.andrade
-- Create date: 11-04-2019
-- Description:	Add new dimension
-- =============================================

IF OBJECT_ID ( 'dbo.uspAddDimResolutionMethods', 'P' ) IS NOT NULL
  DROP PROCEDURE [dbo].[uspAddDimResolutionMethods]
GO

CREATE PROCEDURE [dbo].[uspAddDimResolutionMethods]
AS
  SET NOCOUNT ON;
  BEGIN
    INSERT INTO [dbo].[dim_resolution_methods] ([resolution_method])
    SELECT DISTINCT [tt].[dim_resolution_method_id]
    FROM [dbo].[tmp_fact_tickets] AS tt
    LEFT JOIN [dbo].[dim_resolution_methods] AS dp ON [tt].[dim_resolution_method_id] = [dp].[resolution_method]
    WHERE (([dp].[id] Is NULL) AND ([tt].[dim_resolution_method_id] Is NOT NULL));
  END
GO
