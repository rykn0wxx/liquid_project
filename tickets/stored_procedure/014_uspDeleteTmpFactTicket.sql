USE [Liquid]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	  ariel.c.andrade
-- Create date: 11-04-2019
-- Description:	Clear tmp_fact_tickets
-- =============================================

IF OBJECT_ID ( 'dbo.uspDeleteTmpFactTicket', 'P' ) IS NOT NULL
  DROP PROCEDURE [dbo].[uspDeleteTmpFactTicket]
GO

USE [Liquid]
GO

CREATE PROCEDURE [dbo].[uspDeleteTmpFactTicket]
AS
  SET NOCOUNT ON;
  BEGIN
    DELETE FROM [dbo].[tmp_fact_tickets]
  END
GO
