USE [Liquid]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	  ariel.c.andrade
-- Create date: 11-04-2019
-- Description:	Add new dimension
-- =============================================

IF OBJECT_ID ( 'dbo.uspAddDimTicketTypes', 'P' ) IS NOT NULL
  DROP PROCEDURE [dbo].[uspAddDimTicketTypes]
GO

CREATE PROCEDURE [dbo].[uspAddDimTicketTypes]
AS
  SET NOCOUNT ON;
  BEGIN
    INSERT INTO [dbo].[dim_ticket_types] ([ticket_type])
    SELECT DISTINCT [tt].[dim_ticket_type_id]
    FROM [dbo].[tmp_fact_tickets] AS tt
    LEFT JOIN [dbo].[dim_ticket_types] AS dp ON [tt].[dim_ticket_type_id] = [dp].[ticket_type]
    WHERE (([dp].[id] Is NULL) AND ([tt].[dim_ticket_type_id] Is NOT NULL));
  END
GO
