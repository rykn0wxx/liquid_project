USE [Liquid]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	  ariel.c.andrade
-- Create date: 25-04-2019
-- =============================================

IF OBJECT_ID ( 'dbo.uspInsertTmpToFactTicket', 'P' ) IS NOT NULL
  DROP PROCEDURE [dbo].[uspInsertTmpToFactTicket]
GO

CREATE PROCEDURE [dbo].[uspInsertTmpToFactTicket]
AS
  SET NOCOUNT ON;
  BEGIN

    INSERT INTO [dbo].[fact_tickets] (
      [dim_project_id],
      [ticket_active],
      [ticket_number],
      [ticket_created_at],
      [ticket_resolved_at],
      [ticket_closed_at],
      [ticket_knowledge],
      [dim_ticket_state_id],
      [dim_ticket_type_id],
      [dim_ticket_contact_id],
      [dim_ticket_priority_id],
      [dim_ticket_urgency_id],
      [dim_ticket_impact_id],
      [dim_ticket_category_id],
      [dim_ticket_subcategory_id],
      [dim_business_service_id],
      [dim_configuration_item_id],
      [reasignment_count],
      [dim_resolution_code_id],
      [dim_resolution_method_id],
      [opened_by_id],
      [assignment_group_id],
      [assigned_to_id],
      [resolved_by_id],
      [closed_by_id]
    )

    SELECT [uv].*
    FROM [dbo].[uvTmpFactTicket] AS uv
      LEFT OUTER JOIN [dbo].[fact_tickets] AS f
      ON [uv].[dim_project_id] = [f].[dim_project_id]
        AND [uv].[ticket_number] = [f].[ticket_number]
        AND [uv].[ticket_created_at] = [f].[ticket_created_at]
        AND [uv].[dim_ticket_type_id] = [f].[dim_ticket_type_id]
    WHERE [f].[id] IS NULL

  END
GO
