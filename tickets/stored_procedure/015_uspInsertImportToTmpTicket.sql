USE [Liquid]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	  ariel.c.andrade
-- Create date: 11-04-2019
-- Description:	Data from import_tbl to tmp
-- =============================================

IF OBJECT_ID ( 'dbo.uspInsertImportToTmpTicket', 'P' ) IS NOT NULL
  DROP PROCEDURE [dbo].[uspInsertImportToTmpTicket]
GO

CREATE PROCEDURE [dbo].[uspInsertImportToTmpTicket]
AS
  SET NOCOUNT ON;
  BEGIN

    INSERT INTO [Liquid].[dbo].[tmp_fact_tickets]
    SELECT * FROM [Liquid].[dbo].[import_tbl]

  END
GO
