USE [Liquid]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	  ariel.c.andrade
-- Create date: 11-04-2019
-- Description:	Add new dimension
-- =============================================

IF OBJECT_ID ( 'dbo.uspAddDimGroups', 'P' ) IS NOT NULL
  DROP PROCEDURE [dbo].[uspAddDimGroups]
GO

CREATE PROCEDURE [dbo].[uspAddDimGroups]
AS
  SET NOCOUNT ON;
BEGIN
  INSERT INTO [dbo].[dim_groups] ([group_name], [dim_project_id])
  SELECT DISTINCT [tt].[assignment_group_id], [dmp].[id]
  FROM [dbo].[tmp_fact_tickets] AS tt
  INNER JOIN [dbo].[dim_projects] AS dmp ON [tt].[dim_project_id] = [dmp].[project_name]
  LEFT JOIN [dbo].[dim_groups] AS dp ON [tt].[assignment_group_id] = [dp].[group_name]
  WHERE (([dp].[id] Is NULL) AND ([tt].[assignment_group_id] Is NOT NULL));
END
GO
