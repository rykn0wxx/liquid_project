USE [Liquid]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	  ariel.c.andrade
-- Create date: 16-04-2019
-- =============================================

IF OBJECT_ID ( 'dbo.uspUpdFactTktFromTempTkt', 'P' ) IS NOT NULL
  DROP PROCEDURE [dbo].[uspUpdFactTktFromTempTkt]
GO

USE [Liquid]
GO

CREATE PROCEDURE [dbo].[uspUpdFactTktFromTempTkt]
AS
  SET NOCOUNT ON;
  BEGIN

    UPDATE [dbo].[fact_tickets]
    SET
      [dbo].[fact_tickets].[ticket_active] = [dbo].[uvTmpFactTicket].[ticket_active],
      [dbo].[fact_tickets].[ticket_resolved_at] = [dbo].[uvTmpFactTicket].[ticket_resolved_at],
      [dbo].[fact_tickets].[ticket_closed_at] = [dbo].[uvTmpFactTicket].[ticket_closed_at],
      [dbo].[fact_tickets].[dim_ticket_category_id] = [dbo].[uvTmpFactTicket].[dim_ticket_category_id],
      [dbo].[fact_tickets].[dim_ticket_subcategory_id] = [dbo].[uvTmpFactTicket].[dim_ticket_subcategory_id],
      [dbo].[fact_tickets].[dim_business_service_id] = [dbo].[uvTmpFactTicket].[dim_business_service_id],
      [dbo].[fact_tickets].[dim_configuration_item_id] = [dbo].[uvTmpFactTicket].[dim_configuration_item_id],
      [dbo].[fact_tickets].[reasignment_count] = [dbo].[uvTmpFactTicket].[reasignment_count],
      [dbo].[fact_tickets].[dim_resolution_code_id] = [dbo].[uvTmpFactTicket].[dim_resolution_code_id],
      [dbo].[fact_tickets].[dim_resolution_method_id] = [dbo].[uvTmpFactTicket].[dim_resolution_method_id],
      [dbo].[fact_tickets].[assignment_group_id] = [dbo].[uvTmpFactTicket].[assignment_group_id],
      [dbo].[fact_tickets].[resolved_by_id] = [dbo].[uvTmpFactTicket].[resolved_by_id],
      [dbo].[fact_tickets].[closed_by_id] = [dbo].[uvTmpFactTicket].[closed_by_id]

    FROM [dbo].[fact_tickets]
      INNER JOIN [dbo].[uvTmpFactTicket] ON (
        [dbo].[fact_tickets].[dim_ticket_type_id] = [dbo].[uvTmpFactTicket].[dim_ticket_type_id] AND
        [dbo].[fact_tickets].[ticket_created_at] = [dbo].[uvTmpFactTicket].[ticket_created_at] AND
        [dbo].[fact_tickets].[ticket_number] = [dbo].[uvTmpFactTicket].[ticket_number] AND
        [dbo].[fact_tickets].[dim_project_id] = [dbo].[uvTmpFactTicket].[dim_project_id]
      )

    WHERE (([dbo].[fact_tickets].[id] Is NOT NULL))

  END
GO
