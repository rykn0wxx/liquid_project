USE [Liquid]
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[uvTmpFactTicket]'))
  DROP VIEW [dbo].[uvTmpFactTicket]
GO

USE [Liquid]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[uvTmpFactTicket]
AS
  SELECT
    dp.id AS dim_project_id,
    tt.ticket_active,
    tt.ticket_number,
    tt.ticket_created_at,
    tt.ticket_resolved_at,
    tt.ticket_closed_at,
    IsNumeric(Len(tt.ticket_knowledge)) AS ticket_knowledge,
    dts.id AS dim_ticket_state_id,
    dtt.id AS dim_ticket_type_id,
    dtc.id AS dim_ticket_contact_id,
    dtp.id AS dim_ticket_priority_id,
    dtu.id AS dim_ticket_urgency_id,
    dti.id AS dim_ticket_impact_id,
    dtcc.id AS dim_ticket_category_id,
    dtsc.id AS dim_ticket_subcategory_id,
    dbs.id AS dim_business_service_id,
    dci.id AS dim_configuration_item_id,
    tt.reasignment_count,
    dsc.id AS dim_resolution_code_id,
    dsm.id AS dim_resolution_method_id,
    dtu_opened.id AS opened_by_id,
    dg.id AS assignment_group_id,
    dtu_assigned.id AS assigned_to_id,
    dtu_resolved.id AS resolved_by_id,
    dtu_closed.id AS closed_by_id

  FROM
    dbo.tmp_fact_tickets AS tt
    INNER JOIN dbo.dim_projects AS dp ON (tt.dim_project_id = dp.project_name)
    INNER JOIN dbo.dim_ticket_states AS dts ON (tt.dim_ticket_state_id = dts.ticket_state)
    INNER JOIN dbo.dim_ticket_types AS dtt ON (tt.dim_ticket_type_id = dtt.ticket_type)
    INNER JOIN dbo.dim_ticket_contacts AS dtc ON (tt.dim_ticket_contact_id = dtc.ticket_contact)
    INNER JOIN dbo.dim_ticket_priorities AS dtp ON (tt.dim_ticket_priority_id = dtp.ticket_priority)
    INNER JOIN dbo.dim_ticket_urgencies AS dtu ON (tt.dim_ticket_urgency_id = dtu.ticket_urgency)
    INNER JOIN dbo.dim_ticket_impacts AS dti ON (tt.dim_ticket_impact_id = dti.ticket_impact)
    LEFT OUTER JOIN dbo.dim_ticket_categories AS dtcc ON (tt.dim_ticket_category_id = dtcc.ticket_category)
    LEFT OUTER JOIN dbo.dim_ticket_subcategories AS dtsc ON (tt.dim_ticket_subcategory_id = dtsc.ticket_subcategory)
    LEFT OUTER JOIN dbo.dim_business_services AS dbs ON (tt.dim_business_service_id = dbs.business_service)
    LEFT OUTER JOIN dbo.dim_configuration_items AS dci ON (tt.dim_configuration_item_id = dci.configuration_item)
    LEFT OUTER JOIN dbo.dim_resolution_codes AS dsc ON (tt.dim_resolution_code_id = dsc.resolution_code)
    LEFT OUTER JOIN dbo.dim_resolution_methods AS dsm ON (tt.dim_resolution_method_id = dsm.resolution_method)
    LEFT OUTER JOIN dbo.dim_ticket_users AS dtu_opened ON (tt.opened_by_id = dtu_opened.ticket_user_id AND dp.id = dtu_opened.dim_project_id)
    LEFT OUTER JOIN dbo.dim_groups AS dg ON (tt.assignment_group_id = dg.group_name AND dp.id = dg.dim_project_id)
    LEFT OUTER JOIN dbo.dim_ticket_users AS dtu_assigned ON (tt.assigned_to_id = dtu_assigned.ticket_user_id AND dp.id = dtu_assigned.dim_project_id)
    LEFT OUTER JOIN dbo.dim_ticket_users AS dtu_resolved ON (tt.resolved_by_id = dtu_resolved.ticket_user_id AND dp.id = dtu_resolved.dim_project_id)
    LEFT OUTER JOIN dbo.dim_ticket_users AS dtu_closed ON (tt.closed_by_id = dtu_closed.ticket_user_id AND dp.id = dtu_closed.dim_project_id)

GO
