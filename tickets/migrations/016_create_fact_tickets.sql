USE [Liquid]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fact_tickets]') AND type in (N'U'))
  DROP TABLE [dbo].[fact_tickets]
  GO

USE [Liquid]
GO

CREATE TABLE [dbo].[fact_tickets] (
  [id] INT NOT NULL IDENTITY(1, 1),
  [dim_project_id] INT NOT NULL,
  [ticket_active] BIT NOT NULL,
  [ticket_number] VARCHAR (100) NOT NULL DEFAULT '',
  [ticket_created_at] DATETIME NOT NULL,
  [ticket_resolved_at] DATETIME,
  [ticket_closed_at] DATETIME,
  [ticket_knowledge] BIT,
  [dim_ticket_state_id] INT NOT NULL,
  [dim_ticket_type_id] INT NOT NULL,
  [dim_ticket_contact_id] INT NOT NULL,
  [dim_ticket_priority_id] INT NOT NULL,
  [dim_ticket_urgency_id] INT NOT NULL,
  [dim_ticket_impact_id] INT NOT NULL,
  [dim_ticket_category_id] INT,
  [dim_ticket_subcategory_id] INT,
  [dim_business_service_id] INT,
  [dim_configuration_item_id] INT,
  [reasignment_count] INT NOT NULL DEFAULT 0,
  [dim_resolution_code_id] INT,
  [dim_resolution_method_id] INT,
  [opened_by_id] INT,
  [assignment_group_id] INT,
  [assigned_to_id] INT,
  [resolved_by_id] INT,
  [closed_by_id] INT,
  [created_at] DATETIME,
  CONSTRAINT "PK_fact_tickets" PRIMARY KEY ([id]),
  CONSTRAINT "index_fact_tickets_on_ticket_number" UNIQUE ([dim_project_id], [ticket_number], [ticket_created_at], [dim_ticket_type_id]),
  CONSTRAINT "FK_fact_tickets_dim_project_id"
    FOREIGN KEY ([dim_project_id])
    REFERENCES [dim_projects] ([id]),
  CONSTRAINT "FK_fact_tickets_dim_ticket_state_id"
    FOREIGN KEY ([dim_ticket_state_id])
    REFERENCES [dim_ticket_states] ([id]),
  CONSTRAINT "FK_fact_tickets_dim_ticket_type_id"
    FOREIGN KEY ([dim_ticket_type_id])
    REFERENCES [dim_ticket_types] ([id]),
  CONSTRAINT "FK_fact_tickets_dim_ticket_contact_id"
    FOREIGN KEY ([dim_ticket_contact_id])
    REFERENCES [dim_ticket_contacts] ([id]),
  CONSTRAINT "FK_fact_tickets_dim_ticket_priority_id"
    FOREIGN KEY ([dim_ticket_priority_id])
    REFERENCES [dim_ticket_priorities] ([id]),
  CONSTRAINT "FK_fact_tickets_dim_ticket_urgency_id"
    FOREIGN KEY ([dim_ticket_urgency_id])
    REFERENCES [dim_ticket_urgencies] ([id]),
  CONSTRAINT "FK_fact_tickets_dim_ticket_impact_id"
    FOREIGN KEY ([dim_ticket_impact_id])
    REFERENCES [dim_ticket_impacts] ([id]),
  CONSTRAINT "FK_fact_tickets_dim_ticket_category_id"
    FOREIGN KEY ([dim_ticket_category_id])
    REFERENCES [dim_ticket_categories] ([id]),
  CONSTRAINT "FK_fact_tickets_dim_ticket_subcategory_id"
    FOREIGN KEY ([dim_ticket_subcategory_id])
    REFERENCES [dim_ticket_subcategories] ([id]),
  CONSTRAINT "FK_fact_tickets_dim_business_service_id"
    FOREIGN KEY ([dim_business_service_id])
    REFERENCES [dim_business_services] ([id]),
  CONSTRAINT "FK_fact_tickets_dim_configuration_item_id"
    FOREIGN KEY ([dim_configuration_item_id])
    REFERENCES [dim_configuration_items] ([id]),
  CONSTRAINT "FK_fact_tickets_dim_resolution_code_id"
    FOREIGN KEY ([dim_resolution_code_id])
    REFERENCES [dim_resolution_codes] ([id]),
  CONSTRAINT "FK_fact_tickets_dim_resolution_method_id"
    FOREIGN KEY ([dim_resolution_method_id])
    REFERENCES [dim_resolution_methods] ([id]),
  CONSTRAINT "FK_fact_tickets_opened_by_id"
    FOREIGN KEY ([opened_by_id])
    REFERENCES [dim_ticket_users] ([id]),
  CONSTRAINT "FK_fact_tickets_assignment_group_id"
    FOREIGN KEY ([assignment_group_id])
    REFERENCES [dim_groups] ([id]),
  CONSTRAINT "FK_fact_tickets_assigned_to_id"
    FOREIGN KEY ([assigned_to_id])
    REFERENCES [dim_ticket_users] ([id]),
  CONSTRAINT "FK_fact_tickets_resolved_by_id"
    FOREIGN KEY ([resolved_by_id])
    REFERENCES [dim_ticket_users] ([id]),
  CONSTRAINT "FK_fact_tickets_closed_by_id"
    FOREIGN KEY ([closed_by_id])
    REFERENCES [dim_ticket_users] ([id])
)
GO
