USE [Liquid]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[dim_ticket_contacts]') AND type in (N'U'))
  DROP TABLE [dbo].[dim_ticket_contacts]
  GO

USE [Liquid]
GO

CREATE TABLE [dbo].[dim_ticket_contacts] (
  [id] INT NOT NULL IDENTITY(1, 1),
  [ticket_contact] VARCHAR (100) NOT NULL DEFAULT '',
  [active] BIT NOT NULL DEFAULT 1,
  CONSTRAINT "PK_dim_ticket_contacts" PRIMARY KEY ([id]),
  CONSTRAINT "index_dim_ticket_contacts_on_ticket_contact" UNIQUE ([ticket_contact])
)
GO

INSERT INTO [dbo].[dim_ticket_contacts] ([ticket_contact]) VALUES ('undefined')
GO
