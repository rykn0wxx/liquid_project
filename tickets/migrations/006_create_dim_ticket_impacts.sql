USE [Liquid]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[dim_ticket_impacts]') AND type in (N'U'))
  DROP TABLE [dbo].[dim_ticket_impacts]
  GO

USE [Liquid]
GO

CREATE TABLE [dbo].[dim_ticket_impacts] (
  [id] INT NOT NULL IDENTITY(1, 1),
  [ticket_impact] VARCHAR (100) NOT NULL DEFAULT '',
  [active] BIT NOT NULL DEFAULT 1,
  CONSTRAINT "PK_dim_ticket_impacts" PRIMARY KEY ([id]),
  CONSTRAINT "index_dim_ticket_impacts_on_ticket_impact" UNIQUE ([ticket_impact])
)
GO

INSERT INTO [dbo].[dim_ticket_impacts] ([ticket_impact]) VALUES ('undefined')
GO
