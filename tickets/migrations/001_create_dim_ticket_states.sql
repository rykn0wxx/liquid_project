USE [Liquid]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[dim_ticket_states]') AND type in (N'U'))
  DROP TABLE [dbo].[dim_ticket_states]
  GO

USE [Liquid]
GO

CREATE TABLE [dbo].[dim_ticket_states] (
  [id] INT NOT NULL IDENTITY(1, 1),
  [ticket_state] VARCHAR (100) NOT NULL DEFAULT '',
  [active] BIT NOT NULL DEFAULT 1,
  CONSTRAINT "PK_dim_ticket_states" PRIMARY KEY ([id]),
  CONSTRAINT "index_dim_ticket_states_on_ticket_state" UNIQUE ([ticket_state])
)
GO

INSERT INTO [dbo].[dim_ticket_states] ([ticket_state]) VALUES ('undefined')
GO
