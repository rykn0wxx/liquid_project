USE [Liquid]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[dim_resolution_codes]') AND type in (N'U'))
  DROP TABLE [dbo].[dim_resolution_codes]
  GO

USE [Liquid]
GO

CREATE TABLE [dbo].[dim_resolution_codes] (
  [id] INT NOT NULL IDENTITY(1, 1),
  [resolution_code] VARCHAR (100) NOT NULL DEFAULT '',
  [active] BIT NOT NULL DEFAULT 1,
  CONSTRAINT "PK_dim_resolution_codes" PRIMARY KEY ([id]),
  CONSTRAINT "index_dim_resolution_codes_on_resolution_code" UNIQUE ([resolution_code])
)
GO

INSERT INTO [dbo].[dim_resolution_codes] ([resolution_code]) VALUES ('undefined')
GO
