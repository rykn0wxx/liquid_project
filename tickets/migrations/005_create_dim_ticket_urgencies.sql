USE [Liquid]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[dim_ticket_urgencies]') AND type in (N'U'))
  DROP TABLE [dbo].[dim_ticket_urgencies]
  GO

USE [Liquid]
GO

CREATE TABLE [dbo].[dim_ticket_urgencies] (
  [id] INT NOT NULL IDENTITY(1, 1),
  [ticket_urgency] VARCHAR (100) NOT NULL DEFAULT '',
  [active] BIT NOT NULL DEFAULT 1,
  CONSTRAINT "PK_dim_ticket_urgencies" PRIMARY KEY ([id]),
  CONSTRAINT "index_dim_ticket_urgencies_on_ticket_urgency" UNIQUE ([ticket_urgency])
)
GO

INSERT INTO [dbo].[dim_ticket_urgencies] ([ticket_urgency]) VALUES ('undefined')
GO
