USE [Liquid]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[dim_configuration_items]') AND type in (N'U'))
  DROP TABLE [dbo].[dim_configuration_items]
  GO

USE [Liquid]
GO

CREATE TABLE [dbo].[dim_configuration_items] (
  [id] INT NOT NULL IDENTITY(1, 1),
  [configuration_item] VARCHAR (100) NOT NULL DEFAULT '',
  [active] BIT NOT NULL DEFAULT 1,
  CONSTRAINT "PK_dim_configuration_items" PRIMARY KEY ([id]),
  CONSTRAINT "index_dim_configuration_items_on_configuration_item" UNIQUE ([configuration_item])
)
GO

INSERT INTO [dbo].[dim_configuration_items] ([configuration_item]) VALUES ('undefined')
GO
