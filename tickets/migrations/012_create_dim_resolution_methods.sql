USE [Liquid]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[dim_resolution_methods]') AND type in (N'U'))
  DROP TABLE [dbo].[dim_resolution_methods]
  GO

USE [Liquid]
GO

CREATE TABLE [dbo].[dim_resolution_methods] (
  [id] INT NOT NULL IDENTITY(1, 1),
  [resolution_method] VARCHAR (100) NOT NULL DEFAULT '',
  [active] BIT NOT NULL DEFAULT 1,
  CONSTRAINT "PK_dim_resolution_methods" PRIMARY KEY ([id]),
  CONSTRAINT "index_dim_resolution_methods_on_resolution_method" UNIQUE ([resolution_method])
)
GO

INSERT INTO [dbo].[dim_resolution_methods] ([resolution_method]) VALUES ('undefined')
GO
