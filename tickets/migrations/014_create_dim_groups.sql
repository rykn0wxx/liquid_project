USE [Liquid]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[dim_groups]') AND type in (N'U'))
  DROP TABLE [dbo].[dim_groups]
  GO

USE [Liquid]
GO

CREATE TABLE [dbo].[dim_groups] (
  [id] INT NOT NULL IDENTITY(1, 1),
  [group_name] VARCHAR (100) NOT NULL DEFAULT '',
  [dim_project_id] INT NOT NULL,
  [active] BIT NOT NULL DEFAULT 1,
  CONSTRAINT "PK_dim_groups" PRIMARY KEY ([id]),
  CONSTRAINT "index_dim_groups_on_group_name" UNIQUE ([group_name], [dim_project_id]),
  CONSTRAINT "FK_dim_groups_dim_project_id"
    FOREIGN KEY ([dim_project_id])
    REFERENCES [dim_projects] ([id])
)
GO

INSERT INTO [dbo].[dim_groups]
  (
    [group_name], [dim_project_id]
  )
VALUES
  (
    'undefined', 1
  )

GO
