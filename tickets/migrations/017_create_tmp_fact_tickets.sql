USE [Liquid]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tmp_fact_tickets]') AND type in (N'U'))
  DROP TABLE [dbo].[tmp_fact_tickets]
  GO

USE [Liquid]
GO

CREATE TABLE [dbo].[tmp_fact_tickets] (
  [id] INT NOT NULL IDENTITY(1, 1),
  [dim_project_id] VARCHAR (100),
  [ticket_active] BIT,
  [ticket_number] VARCHAR (100),
  [ticket_created_at] DATETIME,
  [ticket_resolved_at] DATETIME,
  [ticket_closed_at] DATETIME,
  [ticket_knowledge] VARCHAR (100),
  [dim_ticket_state_id] VARCHAR (100),
  [dim_ticket_type_id] VARCHAR (100),
  [dim_ticket_contact_id] VARCHAR (100),
  [dim_ticket_priority_id] VARCHAR (100),
  [dim_ticket_urgency_id] VARCHAR (100),
  [dim_ticket_impact_id] VARCHAR (100),
  [dim_ticket_category_id] VARCHAR (100),
  [dim_ticket_subcategory_id] VARCHAR (100),
  [dim_business_service_id] VARCHAR (100),
  [dim_configuration_item_id] VARCHAR (100),
  [reasignment_count] INT,
  [dim_resolution_code_id] VARCHAR (100),
  [dim_resolution_method_id] VARCHAR (100),
  [opened_by_id] VARCHAR (100),
  [assignment_group_id] VARCHAR (100),
  [assigned_to_id] VARCHAR (100),
  [resolved_by_id] VARCHAR (100),
  [closed_by_id] VARCHAR (100),
  CONSTRAINT "PK_tmp_fact_tickets" PRIMARY KEY ([id])
)
GO
