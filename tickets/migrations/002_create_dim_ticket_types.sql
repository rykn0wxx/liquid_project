USE [Liquid]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[dim_ticket_types]') AND type in (N'U'))
  DROP TABLE [dbo].[dim_ticket_types]
  GO

USE [Liquid]
GO

CREATE TABLE [dbo].[dim_ticket_types] (
  [id] INT NOT NULL IDENTITY(1, 1),
  [ticket_type] VARCHAR (100) NOT NULL DEFAULT '',
  [active] BIT NOT NULL DEFAULT 1,
  CONSTRAINT "PK_dim_ticket_types" PRIMARY KEY ([id]),
  CONSTRAINT "index_dim_ticket_types_on_ticket_type" UNIQUE ([ticket_type])
)
GO

INSERT INTO [dbo].[dim_ticket_types] ([ticket_type]) VALUES ('undefined')
GO
