USE [Liquid]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[dim_ticket_subcategories]') AND type in (N'U'))
  DROP TABLE [dbo].[dim_ticket_subcategories]
  GO

USE [Liquid]
GO

CREATE TABLE [dbo].[dim_ticket_subcategories] (
  [id] INT NOT NULL IDENTITY(1, 1),
  [ticket_subcategory] VARCHAR (100) NOT NULL DEFAULT '',
  [active] BIT NOT NULL DEFAULT 1,
  CONSTRAINT "PK_dim_ticket_subcategories" PRIMARY KEY ([id]),
  CONSTRAINT "index_dim_ticket_subcategories_on_ticket_subcategory" UNIQUE ([ticket_subcategory])
)
GO

INSERT INTO [dbo].[dim_ticket_subcategories] ([ticket_subcategory]) VALUES ('undefined')
GO
