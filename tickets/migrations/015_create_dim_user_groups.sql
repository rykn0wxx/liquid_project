USE [Liquid]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[dim_user_groups]') AND type in (N'U'))
  DROP TABLE [dbo].[dim_user_groups]
  GO

USE [Liquid]
GO

CREATE TABLE [dbo].[dim_user_groups] (
  [id] INT NOT NULL IDENTITY(1, 1),
  [dim_ticket_user_id] INT NOT NULL,
  [dim_group_id] INT NOT NULL,
  [start_date] DATE NOT NULL,
  [end_date] DATE,
  [active] BIT NOT NULL DEFAULT 1,
  CONSTRAINT "PK_dim_user_groups" PRIMARY KEY ([id]),
  CONSTRAINT "index_dim_user_groups_on_user_group_name" UNIQUE ([dim_ticket_user_id], [dim_group_id]),
  CONSTRAINT "FK_dim_user_groups_dim_ticket_user_id"
    FOREIGN KEY ([dim_ticket_user_id])
    REFERENCES [dim_ticket_users] ([id]),
  CONSTRAINT "FK_dim_user_groups_dim_group_id"
    FOREIGN KEY ([dim_group_id])
    REFERENCES [dim_groups] ([id])
)
GO

INSERT INTO [dbo].[dim_user_groups]
  (
    [dim_ticket_user_id], [dim_group_id], [start_date]
  )
VALUES
  (
    1, 1, '2000-01-01'
  )

GO
