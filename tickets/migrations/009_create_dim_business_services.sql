USE [Liquid]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[dim_business_services]') AND type in (N'U'))
  DROP TABLE [dbo].[dim_business_services]
  GO

USE [Liquid]
GO

CREATE TABLE [dbo].[dim_business_services] (
  [id] INT NOT NULL IDENTITY(1, 1),
  [business_service] VARCHAR (100) NOT NULL DEFAULT '',
  [active] BIT NOT NULL DEFAULT 1,
  CONSTRAINT "PK_dim_business_services" PRIMARY KEY ([id]),
  CONSTRAINT "index_dim_business_services_on_business_service" UNIQUE ([business_service])
)
GO

INSERT INTO [dbo].[dim_business_services] ([business_service]) VALUES ('undefined')
GO
