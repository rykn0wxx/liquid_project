USE [Liquid]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[dim_ticket_categories]') AND type in (N'U'))
  DROP TABLE [dbo].[dim_ticket_categories]
  GO

USE [Liquid]
GO

CREATE TABLE [dbo].[dim_ticket_categories] (
  [id] INT NOT NULL IDENTITY(1, 1),
  [ticket_category] VARCHAR (100) NOT NULL DEFAULT '',
  [active] BIT NOT NULL DEFAULT 1,
  CONSTRAINT "PK_dim_ticket_categories" PRIMARY KEY ([id]),
  CONSTRAINT "index_dim_ticket_categories_on_ticket_category" UNIQUE ([ticket_category])
)
GO

INSERT INTO [dbo].[dim_ticket_categories] ([ticket_category]) VALUES ('undefined')
GO
