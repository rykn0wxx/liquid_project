USE [Liquid]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[dim_ticket_priorities]') AND type in (N'U'))
  DROP TABLE [dbo].[dim_ticket_priorities]
  GO

USE [Liquid]
GO

CREATE TABLE [dbo].[dim_ticket_priorities] (
  [id] INT NOT NULL IDENTITY(1, 1),
  [ticket_priority] VARCHAR (100) NOT NULL DEFAULT '',
  [active] BIT NOT NULL DEFAULT 1,
  CONSTRAINT "PK_dim_ticket_priorities" PRIMARY KEY ([id]),
  CONSTRAINT "index_dim_ticket_priorities_on_ticket_priority" UNIQUE ([ticket_priority])
)
GO

INSERT INTO [dbo].[dim_ticket_priorities] ([ticket_priority]) VALUES ('undefined')
GO
