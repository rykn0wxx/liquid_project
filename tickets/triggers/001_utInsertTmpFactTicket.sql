USE [Liquid]
GO

-- =============================================
-- Author:		ariel.c.andrade
-- Create date: 15-04-2019
-- Description:	After insert into tmp_fact
-- =============================================

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[UpdateTktDimensions]'))
  DROP TRIGGER [dbo].[UpdateTktDimensions]
GO

USE [Liquid]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[UpdateTktDimensions]
  ON [dbo].[tmp_fact_tickets]
  AFTER INSERT

AS

BEGIN
  SET NOCOUNT ON;
  EXEC [dbo].[uspAddDimTicketStates]
  EXEC [dbo].[uspAddDimTicketTypes]
  EXEC [dbo].[uspAddDimTicketContacts]
  EXEC [dbo].[uspAddDimTicketPriorities]
  EXEC [dbo].[uspAddDimTicketUrgencies]
  EXEC [dbo].[uspAddDimTicketImpacts]
  EXEC [dbo].[uspAddDimTicketCategories]
  EXEC [dbo].[uspAddDimTicketSubcategories]
  EXEC [dbo].[uspAddDimBusinessServices]
  EXEC [dbo].[uspAddDimConfigurationItems]
  EXEC [dbo].[uspAddDimResolutionCodes]
  EXEC [dbo].[uspAddDimResolutionMethods]
  EXEC [dbo].[uspAddDimGroups]
END
GO
