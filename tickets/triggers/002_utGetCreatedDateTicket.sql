USE [Liquid]
GO

-- =============================================
-- Author:		ariel.c.andrade
-- Create date: 16-04-2019
-- Description:	Update created_at for tickets
-- =============================================

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[UpdateTicketCreatedAt]'))
  DROP TRIGGER [dbo].[UpdateTicketCreatedAt]
GO

USE [Liquid]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[UpdateTicketCreatedAt]
  ON [dbo].[fact_tickets]
  AFTER INSERT

AS

BEGIN
  SET NOCOUNT ON;
  DECLARE @getDate DATETIME = GETDATE()

  UPDATE
    [dbo].[fact_tickets]
  SET
    [created_at] = @getDate
  FROM
    [dbo].[fact_tickets] AS f
    INNER JOIN INSERTED AS i ON ([f].[id] = [i].[id])
    LEFT OUTER JOIN DELETED AS d ON ([i].[id] = [d].[id])
  WHERE
    [d].[id] Is Null

END
GO
