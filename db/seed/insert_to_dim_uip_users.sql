USE [Liquid]
GO

INSERT INTO [dbo].[dim_emp_users] (emp_id, login_id, first_name, last_name, hire_date, active)
SELECT
  t.emp_id, t.login_id, t.first_name, t.last_name, t.hire_date, 1
FROM
  [dbo].[tt_dim_emp_users] AS t
GO

BEGIN

  UPDATE te
  SET te.supervisor_id = sqry.manager_id
  FROM
    [dbo].[dim_emp_users] AS te
    INNER JOIN [dbo].[tt_dim_emp_users] AS tdeu
      ON (tdeu.emp_id = te.emp_id AND tdeu.login_id = te.login_id AND tdeu.first_name = te.first_name AND tdeu.last_name = te.last_name AND tdeu.hire_date = te.hire_date)
    INNER JOIN (
      SELECT
        aa.manager_name, ttee.id AS manager_id
      FROM
        (SELECT
          DISTINCT
          t.manager_name,
          SUBSTRING(t.manager_name, 1, CHARINDEX(',', t.manager_name) - 1) AS lname,
          SUBSTRING(t.manager_name, CHARINDEX(',', t.manager_name) + 1, LEN(t.manager_name)) AS fname
        FROM
          [dbo].[tt_dim_emp_users] AS t) AS aa
        INNER JOIN [dbo].[dim_emp_users] AS ttee
          ON (aa.lname = ttee.last_name AND aa.fname = ttee.first_name)
    ) AS sqry
      ON (tdeu.manager_name = sqry.manager_name)

END
