USE [Liquid]
GO

BEGIN TRAN
  INSERT INTO [dbo].[dim_regions] ([region_name], [active])
  VALUES
    ('Global', 1),
    ('Africa', 1),
    ('Asia', 1),
    ('Europe', 1),
    ('North America', 1),
    ('Oceania', 1),
    ('South America', 1)

COMMIT
