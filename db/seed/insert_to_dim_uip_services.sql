USE [Liquid]
GO

BEGIN
  insert into dbo.dim_uip_services
  select
    distinct
    t.SERVICE_ID AS service_id,
    ds.id AS dim_call_service_id,
    dl.id AS dim_language_id,
    dc.id AS dim_client_id,
    1 AS active

  from
    dbo.tt_tbl AS t
    inner join dbo.dim_clients AS dc on (t.CLIENT_NAME = dc.client_name)
    inner join dbo.dim_call_services AS ds on (t.SERVICE_NAME = ds.call_service)
    inner join dbo.dim_languages AS dl on (t.langauge_name = dl.language_name)
    left outer join dbo.dim_uip_services AS dup
      on (t.service_id = dup.service_id AND ds.id = dup.dim_call_service_id
      AND dl.id = dup.dim_language_id AND dc.id = dup.dim_client_id)
  where
    (
      (dup.id Is Null)
      AND
      (t.SERVICE_ID Is Not NUll)
    )

END
