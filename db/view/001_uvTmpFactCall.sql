USE [Liquid]
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[uvTmpFactCall]'))
  DROP VIEW [dbo].[uvTmpFactCall]
GO

USE [Liquid]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[uvTmpFactCall]
AS
  SELECT
    [dc].[id] AS dim_client_id,
    [dt].[id] AS dim_timezone_id,
    [dcc].[id] AS dim_call_category_id,
    [dct].[id] AS dim_call_type_id,
    [dca].[id] AS dim_call_action_id,
    [dcar].[id] AS dim_call_action_reason_id,
    [dcd].[id] AS dim_call_disp_id,
    [dcs].[id] AS dim_call_service_id,
    [duu].[id] AS dim_uip_user_id,
    [dip1].[id] AS dim_ivr_param1_id,
    [dip2].[id] AS dim_ivr_param2_id,
    [dip3].[id] AS dim_ivr_param3_id,
    [dip4].[id] AS dim_ivr_param4_id,
    [dip9].[id] AS dim_ivr_param9_id,
    [dip10].[id] AS dim_ivr_param10_id,
    [dip16].[id] AS dim_ivr_param16_id,
    [dip18].[id] AS dim_ivr_param18_id,
    [dip19].[id] AS dim_ivr_param19_id,
    [tt].[tmp_call] AS call_id,
    [tt].[tmp_seq] AS call_seq,
    [tt].[tmp_dialed_num] AS call_dialed_num,
    [tt].[tmp_answer_date] AS call_answer_date,
    [tt].[tmp_end_date] AS call_end_date,
    [tt].[tmp_end_date_tz] AS call_end_date_tz,
    [tt].[tmp_start_date] AS call_start_date,
    [tt].[tmp_start_date_tz] AS call_start_date_tz,
    [tt].[tmp_wrap_end_date] AS call_wrap_end_date,
    [tt].[tmp_hold_number] AS call_hold_number,
    [tt].[tmp_queue_time] AS call_queue_time,
    [tt].[tmp_talk_time] AS call_talk_time,
    [tt].[tmp_hold_time] AS call_hold_time,
    [tt].[tmp_wrap_time] AS call_wrap_time

  FROM
    [dbo].[tmp_fact_call_details] AS [tt]
    INNER JOIN [dbo].[dim_projects] AS dp ON ([tt].[tmp_parent_project] = [dp].[project_name])
    INNER JOIN [dbo].[dim_clients] AS dc ON ([tt].[tmp_project_name] = [dc].[client_name])
    INNER JOIN [dbo].[dim_timezones] AS dt ON ([tt].[tmp_timezone] = [dt].[timezone_name])
    INNER JOIN [dbo].[dim_call_categories] AS dcc ON ([tt].[tmp_call_category] = [dcc].[call_category])
    INNER JOIN [dbo].[dim_call_types] AS dct ON ([tt].[tmp_call_type] = [dct].[call_type])
    INNER JOIN [dbo].[dim_call_actions] AS dca ON ([tt].[tmp_call_action] = [dca].[call_action])
    INNER JOIN [dbo].[dim_call_action_reasons] AS dcar ON ([tt].[tmp_call_action_reason] = [dcar].[call_action_reason])
    LEFT OUTER JOIN [dbo].[dim_call_disps] AS dcd ON ([tt].[tmp_disp] = [dcd].[call_disp])
    INNER JOIN [dbo].[dim_call_services] AS dcs ON ([tt].[tmp_service_name] = [dcs].[call_service])
    LEFT OUTER JOIN [dbo].[dim_uip_users] AS duu ON ([tt].[tmp_user_id] = [duu].[uip_id])
    LEFT OUTER JOIN [dbo].[dim_ivr_params] AS dip1 ON ([tt].[tmp_ivr_param_1] = [dip1].[ivr_param] AND 1 = [dip1].[ivr_code] AND [dp].[id] = [dip1].[dim_project_id])
    LEFT OUTER JOIN [dbo].[dim_ivr_params] AS dip2 ON ([tt].[tmp_ivr_param_2] = [dip2].[ivr_param] AND 2 = [dip2].[ivr_code] AND [dp].[id] = [dip2].[dim_project_id])
    LEFT OUTER JOIN [dbo].[dim_ivr_params] AS dip3 ON ([tt].[tmp_ivr_param_3] = [dip3].[ivr_param] AND 3 = [dip3].[ivr_code] AND [dp].[id] = [dip3].[dim_project_id])
    LEFT OUTER JOIN [dbo].[dim_ivr_params] AS dip4 ON ([tt].[tmp_ivr_param_4] = [dip4].[ivr_param] AND 4 = [dip4].[ivr_code] AND [dp].[id] = [dip4].[dim_project_id])
    LEFT OUTER JOIN [dbo].[dim_ivr_params] AS dip9 ON ([tt].[tmp_ivr_param_9] = [dip9].[ivr_param] AND 9 = [dip9].[ivr_code] AND [dp].[id] = [dip9].[dim_project_id])
    LEFT OUTER JOIN [dbo].[dim_ivr_params] AS dip10 ON ([tt].[tmp_ivr_param_10] = [dip10].[ivr_param] AND 10 = [dip10].[ivr_code] AND [dp].[id] = [dip10].[dim_project_id])
    LEFT OUTER JOIN [dbo].[dim_ivr_params] AS dip16 ON ([tt].[tmp_ivr_param_16] = [dip16].[ivr_param] AND 16 = [dip16].[ivr_code] AND [dp].[id] = [dip16].[dim_project_id])
    LEFT OUTER JOIN [dbo].[dim_ivr_params] AS dip18 ON ([tt].[tmp_ivr_param_18] = [dip18].[ivr_param] AND 18 = [dip18].[ivr_code] AND [dp].[id] = [dip18].[dim_project_id])
    LEFT OUTER JOIN [dbo].[dim_ivr_params] AS dip19 ON ([tt].[tmp_ivr_param_19] = [dip19].[ivr_param] AND 19 = [dip19].[ivr_code] AND [dp].[id] = [dip19].[dim_project_id])

GO