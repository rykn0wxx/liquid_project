USE [Liquid]
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[uvFactCallDetails]'))
  DROP VIEW [dbo].[uvFactCallDetails]
GO

USE [Liquid]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[uvFactCallDetails]
AS
  SELECT
    [f].[id],
    [dp].[project_name],
    [dc].[client_name],
    [dt].[timezone_name],
    [dcc].[call_category],
    [dct].[call_type],
    [dca].[call_action],
    [dcar].[call_action_reason],
    [dcd].[call_disp],
    [dcs].[call_service],
    [duu].[uip_id],
    [deu].[first_name],
    [deu].[last_name],
    [dip1].[ivr_param] AS ivr_param1,
    [dip2].[ivr_param] AS ivr_param2,
    [dip3].[ivr_param] AS ivr_param3,
    [dip4].[ivr_param] AS ivr_param4,
    [dip9].[ivr_param] AS ivr_param9,
    [dip10].[ivr_param] AS ivr_param10,
    [dip16].[ivr_param] AS ivr_param16,
    [dip18].[ivr_param] AS ivr_param18,
    [dip19].[ivr_param] AS ivr_param19,
    [f].[call_id],
    [f].[call_seq],
    [f].[call_dialed_num],
    [f].[call_answer_date],
    [f].[call_end_date],
    [f].[call_end_date_tz],
    [f].[call_start_date],
    [f].[call_start_date_tz],
    [f].[call_wrap_end_date],
    [f].[call_hold_number],
    [f].[call_queue_time],
    [f].[call_talk_time],
    [f].[call_hold_time],
    [f].[call_wrap_time]

  FROM
    [dbo].[fact_call_details] AS f
    INNER JOIN [dbo].[dim_clients] AS dc ON ([f].[dim_client_id] = [dc].[id])
    INNER JOIN [dbo].[dim_projects] AS dp ON ([dc].[dim_project_id] = [dp].[id])
    INNER JOIN [dbo].[dim_timezones] AS dt ON ([f].[dim_timezone_id] = [dt].[id])
    INNER JOIN [dbo].[dim_call_categories] AS dcc ON ([f].[dim_call_category_id] = [dcc].[id])
    INNER JOIN [dbo].[dim_call_types] AS dct ON ([f].[dim_call_type_id] = [dct].[id])
    INNER JOIN [dbo].[dim_call_actions] AS dca ON ([f].[dim_call_action_id] = [dca].[id])
    INNER JOIN [dbo].[dim_call_action_reasons] AS dcar ON ([f].[dim_call_action_reason_id] = [dcar].[id])
    LEFT OUTER JOIN [dbo].[dim_call_disps] AS dcd ON ([f].[dim_call_disp_id] = [dcd].[id])
    INNER JOIN [dbo].[dim_call_services] AS dcs ON ([f].[dim_call_service_id] = [dcs].[id])
    LEFT OUTER JOIN [dbo].[dim_uip_users] AS duu ON ([f].[dim_uip_user_id] = [duu].[id])
    LEFT JOIN [dbo].[dim_emp_users] AS deu ON ([duu].[dim_emp_user_id] = [deu].[id])
    LEFT OUTER JOIN [dbo].[dim_ivr_params] AS dip1 ON ([f].[dim_ivr_param1_id] = [dip1].[id] AND 1 = [dip1].[ivr_code] AND [dp].[id] = [dip1].[dim_project_id])
    LEFT OUTER JOIN [dbo].[dim_ivr_params] AS dip2 ON ([f].[dim_ivr_param2_id] = [dip2].[id] AND 2 = [dip2].[ivr_code] AND [dp].[id] = [dip2].[dim_project_id])
    LEFT OUTER JOIN [dbo].[dim_ivr_params] AS dip3 ON ([f].[dim_ivr_param3_id] = [dip3].[id] AND 3 = [dip3].[ivr_code] AND [dp].[id] = [dip3].[dim_project_id])
    LEFT OUTER JOIN [dbo].[dim_ivr_params] AS dip4 ON ([f].[dim_ivr_param4_id] = [dip4].[id] AND 4 = [dip4].[ivr_code] AND [dp].[id] = [dip4].[dim_project_id])
    LEFT OUTER JOIN [dbo].[dim_ivr_params] AS dip9 ON ([f].[dim_ivr_param9_id] = [dip9].[id] AND 9 = [dip9].[ivr_code] AND [dp].[id] = [dip9].[dim_project_id])
    LEFT OUTER JOIN [dbo].[dim_ivr_params] AS dip10 ON ([f].[dim_ivr_param10_id] = [dip10].[id] AND 10 = [dip10].[ivr_code] AND [dp].[id] = [dip10].[dim_project_id])
    LEFT OUTER JOIN [dbo].[dim_ivr_params] AS dip16 ON ([f].[dim_ivr_param16_id] = [dip16].[id] AND 16 = [dip16].[ivr_code] AND [dp].[id] = [dip16].[dim_project_id])
    LEFT OUTER JOIN [dbo].[dim_ivr_params] AS dip18 ON ([f].[dim_ivr_param18_id] = [dip18].[id] AND 18 = [dip18].[ivr_code] AND [dp].[id] = [dip18].[dim_project_id])
    LEFT OUTER JOIN [dbo].[dim_ivr_params] AS dip19 ON ([f].[dim_ivr_param19_id] = [dip19].[id] AND 19 = [dip19].[ivr_code] AND [dp].[id] = [dip19].[dim_project_id])

GO
