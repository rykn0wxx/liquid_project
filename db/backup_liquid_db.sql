USE [Liquid]
GO

BACKUP DATABASE Liquid
TO DISK = 'C:\mud\codes\liquid_project\Liquid.Bak'
  WITH FORMAT,
    MEDIANAME = 'C_LiquidDatabaseBackups',
    NAME = 'Full Backup of Liquid';
GO
