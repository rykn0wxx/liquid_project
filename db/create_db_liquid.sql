USE [master]
GO

/******	Object: Database [Liquid] Start	******/

/******	Drop Database if Exists	******/
IF  EXISTS (SELECT name
FROM sys.databases
WHERE name = N'Liquid')
	DROP DATABASE [Liquid]
	GO

/******	Create Database	******/
CREATE DATABASE [Liquid]
GO

/******	Object: Database [Liquid] End	******/
