USE [Liquid]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	  ariel.c.andrade
-- Create date: 24-03-2019
-- =============================================

IF OBJECT_ID ( 'dbo.uspAddDimCallType', 'P' ) IS NOT NULL
  DROP PROCEDURE [dbo].[uspAddDimCallType]
GO

CREATE PROCEDURE [dbo].[uspAddDimCallType]
AS
BEGIN
  SET NOCOUNT ON;

  INSERT INTO [dbo].[dim_call_types] ([call_type])
  SELECT DISTINCT [tt].[tmp_call_type]
  FROM [dbo].[tmp_fact_call_details] AS tt
  LEFT JOIN [dbo].[dim_call_types] AS dp ON [tt].[tmp_call_type] = [dp].[call_type]
  WHERE (([dp].[id] Is NULL) AND ([tt].[tmp_call_type] Is NOT NULL));
END
GO
