USE [Liquid]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	  ariel.c.andrade
-- Create date: 24-03-2019
-- =============================================

IF OBJECT_ID ( 'dbo.uspAddDimIvrParam2', 'P' ) IS NOT NULL
  DROP PROCEDURE [dbo].[uspAddDimIvrParam2]
GO

CREATE PROCEDURE [dbo].[uspAddDimIvrParam2]
AS
BEGIN
  SET NOCOUNT ON;

  INSERT INTO [dbo].[dim_ivr_params] ([ivr_param], [ivr_code], [dim_project_id])
  SELECT DISTINCT [tt].[tmp_ivr_param_2], 2, [dimp].[id]
  FROM [dbo].[tmp_fact_call_details] AS tt
  INNER JOIN [dbo].[dim_projects] AS dimp ON [tt].[tmp_parent_project] = [dimp].[project_name]
  LEFT JOIN [dbo].[dim_ivr_params] AS dp ON [tt].[tmp_ivr_param_2] = [dp].[ivr_param] AND [dimp].[id] = [dp].[dim_project_id]
  WHERE (([dp].[id] Is NULL) AND ([tt].[tmp_ivr_param_2] Is NOT NULL));
END
GO
