USE [Liquid]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	  ariel.c.andrade
-- Create date: 24-03-2019
-- =============================================

IF OBJECT_ID ( 'dbo.uspAddDimTimezone', 'P' ) IS NOT NULL
  DROP PROCEDURE [dbo].[uspAddDimTimezone]
GO

CREATE PROCEDURE [dbo].[uspAddDimTimezone]
AS
BEGIN
  SET NOCOUNT ON;

  INSERT INTO [dbo].[dim_timezones] ([timezone_name])
  SELECT DISTINCT [tt].[tmp_timezone]
  FROM [dbo].[tmp_fact_call_details] AS tt
  LEFT JOIN [dbo].[dim_timezones] AS dp ON [tt].[tmp_timezone] = [dp].[timezone_name]
  WHERE (([dp].[id] Is NULL) AND ([tt].[tmp_timezone] Is NOT NULL));
END
GO
