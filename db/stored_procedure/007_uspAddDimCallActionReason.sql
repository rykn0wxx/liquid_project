USE [Liquid]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	  ariel.c.andrade
-- Create date: 24-03-2019
-- =============================================

IF OBJECT_ID ( 'dbo.uspAddDimCallActionReason', 'P' ) IS NOT NULL
  DROP PROCEDURE [dbo].[uspAddDimCallActionReason]
GO

CREATE PROCEDURE [dbo].[uspAddDimCallActionReason]
AS
BEGIN
  SET NOCOUNT ON;

  INSERT INTO [dbo].[dim_call_action_reasons] ([call_action_reason])
  SELECT DISTINCT [tt].[tmp_call_action_reason]
  FROM [dbo].[tmp_fact_call_details] AS tt
  LEFT JOIN [dbo].[dim_call_action_reasons] AS dp ON [tt].[tmp_call_action_reason] = [dp].[call_action_reason]
  WHERE (([dp].[id] Is NULL) AND ([tt].[tmp_call_action_reason] Is NOT NULL));
END
GO
