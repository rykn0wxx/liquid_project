USE [Liquid]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	  ariel.c.andrade
-- Create date: 24-03-2019
-- =============================================

IF OBJECT_ID ( 'dbo.uspResetTmpFactIdentity', 'P' ) IS NOT NULL
  DROP PROCEDURE [dbo].[uspResetTmpFactIdentity]
GO

CREATE PROCEDURE [dbo].[uspResetTmpFactIdentity]
AS
BEGIN
  SET NOCOUNT ON;

  DBCC CHECKIDENT('tmp_fact_call_details', RESEED, 0)

END
GO
