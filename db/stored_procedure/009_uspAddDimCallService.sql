USE [Liquid]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	  ariel.c.andrade
-- Create date: 24-03-2019
-- =============================================

IF OBJECT_ID ( 'dbo.uspAddDimCallService', 'P' ) IS NOT NULL
  DROP PROCEDURE [dbo].[uspAddDimCallService]
GO

CREATE PROCEDURE [dbo].[uspAddDimCallService]
AS
BEGIN
  SET NOCOUNT ON;

  INSERT INTO [dbo].[dim_call_services] ([call_service])
  SELECT DISTINCT [tt].[tmp_service_name]
  FROM [dbo].[tmp_fact_call_details] AS tt
  LEFT JOIN [dbo].[dim_call_services] AS dp ON [tt].[tmp_service_name] = [dp].[call_service]
  WHERE (([dp].[id] Is NULL) AND ([tt].[tmp_service_name] Is NOT NULL));
END
GO
