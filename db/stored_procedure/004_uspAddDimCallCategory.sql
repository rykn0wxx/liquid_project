USE [Liquid]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	  ariel.c.andrade
-- Create date: 24-03-2019
-- =============================================

IF OBJECT_ID ( 'dbo.uspAddDimCallCategory', 'P' ) IS NOT NULL
  DROP PROCEDURE [dbo].[uspAddDimCallCategory]
GO

CREATE PROCEDURE [dbo].[uspAddDimCallCategory]
AS
BEGIN
  SET NOCOUNT ON;

  INSERT INTO [dbo].[dim_call_categories] ([call_category])
  SELECT DISTINCT [tt].[tmp_call_category]
  FROM [dbo].[tmp_fact_call_details] AS tt
  LEFT JOIN [dbo].[dim_call_categories] AS dp ON [tt].[tmp_call_category] = [dp].[call_category]
  WHERE (([dp].[id] Is NULL) AND ([tt].[tmp_call_category] Is NOT NULL));
END
GO
