USE [Liquid]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	  ariel.c.andrade
-- Create date: 24-03-2019
-- =============================================

IF OBJECT_ID ( 'dbo.uspAddDimProject', 'P' ) IS NOT NULL
  DROP PROCEDURE [dbo].[uspAddDimProject]
GO

CREATE PROCEDURE [dbo].[uspAddDimProject]
AS
BEGIN
  SET NOCOUNT ON;

  INSERT INTO [dbo].[dim_projects] ([project_name])
  SELECT DISTINCT [tt].[tmp_parent_project]
  FROM [dbo].[tmp_fact_call_details] AS tt
  LEFT JOIN [dbo].[dim_projects] AS dp ON [tt].[tmp_parent_project] = [dp].[project_name]
  WHERE (([dp].[id] Is NULL) AND ([tt].[tmp_parent_project] Is NOT NULL));
END
GO
