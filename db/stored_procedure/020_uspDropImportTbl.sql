USE [Liquid]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	  ariel.c.andrade
-- Create date: 24-03-2019
-- =============================================

IF OBJECT_ID ( 'dbo.uspDropImportTbl', 'P' ) IS NOT NULL
  DROP PROCEDURE [dbo].[uspDropImportTbl]
GO

CREATE PROCEDURE [dbo].[uspDropImportTbl]
AS
BEGIN
  SET NOCOUNT ON;

  IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[import_tbl]') AND type in (N'U'))
    DROP TABLE [dbo].[import_tbl]

END
GO
