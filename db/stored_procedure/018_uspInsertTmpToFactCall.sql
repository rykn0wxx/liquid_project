USE [Liquid]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	  ariel.c.andrade
-- Create date: 24-03-2019
-- =============================================

IF OBJECT_ID ( 'dbo.uspInsertTmpToFactCall', 'P' ) IS NOT NULL
  DROP PROCEDURE [dbo].[uspInsertTmpToFactCall]
GO

CREATE PROCEDURE [dbo].[uspInsertTmpToFactCall]
AS
BEGIN
  SET NOCOUNT ON;

  INSERT INTO [dbo].[fact_call_details] (
    [dim_client_id],
    [dim_timezone_id],
    [dim_call_category_id],
    [dim_call_type_id],
    [dim_call_action_id],
    [dim_call_action_reason_id],
    [dim_call_disp_id],
    [dim_call_service_id],
    [dim_uip_user_id],
    [dim_ivr_param1_id],
    [dim_ivr_param2_id],
    [dim_ivr_param3_id],
    [dim_ivr_param4_id],
    [dim_ivr_param9_id],
    [dim_ivr_param10_id],
    [dim_ivr_param16_id],
    [dim_ivr_param18_id],
    [dim_ivr_param19_id],
    [call_id],
    [call_seq],
    [call_dialed_num],
    [call_answer_date],
    [call_end_date],
    [call_end_date_tz],
    [call_start_date],
    [call_start_date_tz],
    [call_wrap_end_date],
    [call_hold_number],
    [call_queue_time],
    [call_talk_time],
    [call_hold_time],
    [call_wrap_time]
  )

  SELECT [uv].*
  FROM [dbo].[uvTmpFactCall] AS uv
    LEFT OUTER JOIN [dbo].[fact_call_details] AS f
    ON [uv].[call_start_date] = [f].[call_start_date]
      AND [uv].[call_end_date] = [f].[call_end_date]
      AND [uv].[call_seq] = [f].[call_seq]
      AND [uv].[call_id] = [f].[call_id]
      AND [uv].[dim_call_action_id] = [f].[dim_call_action_id]
      AND [uv].[dim_call_type_id] = [f].[dim_call_type_id]
      AND [uv].[dim_call_category_id] = [f].[dim_call_category_id]
      AND [uv].[dim_client_id] = [f].[dim_client_id]
  WHERE [f].[id] IS NULL
END
GO
