USE [Liquid]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	  ariel.c.andrade
-- Create date: 24-03-2019
-- =============================================

IF OBJECT_ID ( 'dbo.uspAddDimCallDisp', 'P' ) IS NOT NULL
  DROP PROCEDURE [dbo].[uspAddDimCallDisp]
GO

CREATE PROCEDURE [dbo].[uspAddDimCallDisp]
AS
BEGIN
  SET NOCOUNT ON;

  INSERT INTO [dbo].[dim_call_disps] ([call_disp])
  SELECT DISTINCT [tt].[tmp_disp]
  FROM [dbo].[tmp_fact_call_details] AS tt
  LEFT JOIN [dbo].[dim_call_disps] AS dp ON [tt].[tmp_disp] = [dp].[call_disp]
  WHERE (([dp].[id] Is NULL) AND ([tt].[tmp_disp] Is NOT NULL));
END
GO
