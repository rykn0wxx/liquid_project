USE [Liquid]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	  ariel.c.andrade
-- Create date: 24-03-2019
-- =============================================

IF OBJECT_ID ( 'dbo.uspInsertImportToTmp', 'P' ) IS NOT NULL
  DROP PROCEDURE [dbo].[uspInsertImportToTmp]
GO

CREATE PROCEDURE [dbo].[uspInsertImportToTmp]
AS
BEGIN
  SET NOCOUNT ON;
  
  INSERT INTO [Liquid].[dbo].[tmp_fact_call_details]
  SELECT * FROM [Liquid].[dbo].[import_tbl]

END
GO
