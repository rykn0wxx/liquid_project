USE [Liquid]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	  ariel.c.andrade
-- Create date: 24-03-2019
-- =============================================

IF OBJECT_ID ( 'dbo.uspDeleteTmpFactCall', 'P' ) IS NOT NULL
  DROP PROCEDURE [dbo].[uspDeleteTmpFactCall]
GO

USE [Liquid]
GO

CREATE PROCEDURE [dbo].[uspDeleteTmpFactCall]
AS
BEGIN
  SET NOCOUNT ON;
  DELETE FROM [dbo].[tmp_fact_call_details]
END
GO
