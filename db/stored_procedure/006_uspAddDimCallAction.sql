USE [Liquid]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	  ariel.c.andrade
-- Create date: 24-03-2019
-- =============================================

IF OBJECT_ID ( 'dbo.uspAddDimCallAction', 'P' ) IS NOT NULL
  DROP PROCEDURE [dbo].[uspAddDimCallAction]
GO

CREATE PROCEDURE [dbo].[uspAddDimCallAction]
AS
BEGIN
  SET NOCOUNT ON;

  INSERT INTO [dbo].[dim_call_actions] ([call_action])
  SELECT DISTINCT [tt].[tmp_call_action]
  FROM [dbo].[tmp_fact_call_details] AS tt
  LEFT JOIN [dbo].[dim_call_actions] AS dp ON [tt].[tmp_call_action] = [dp].[call_action]
  WHERE (([dp].[id] Is NULL) AND ([tt].[tmp_call_action] Is NOT NULL));
END
GO
