USE [Liquid]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	  ariel.c.andrade
-- Create date: 24-03-2019
-- =============================================

IF OBJECT_ID ( 'dbo.uspAddDimIvrParam3', 'P' ) IS NOT NULL
  DROP PROCEDURE [dbo].[uspAddDimIvrParam3]
GO

CREATE PROCEDURE [dbo].[uspAddDimIvrParam3]
AS
BEGIN
  SET NOCOUNT ON;

  INSERT INTO [dbo].[dim_ivr_params] ([ivr_param], [ivr_code], [dim_project_id])
  SELECT DISTINCT [tt].[tmp_ivr_param_3], 3, [dimp].[id]
  FROM [dbo].[tmp_fact_call_details] AS tt
  INNER JOIN [dbo].[dim_projects] AS dimp ON [tt].[tmp_parent_project] = [dimp].[project_name]
  LEFT JOIN [dbo].[dim_ivr_params] AS dp ON [tt].[tmp_ivr_param_3] = [dp].[ivr_param] AND [dimp].[id] = [dp].[dim_project_id]
  WHERE (([dp].[id] Is NULL) AND ([tt].[tmp_ivr_param_3] Is NOT NULL));
END
GO
