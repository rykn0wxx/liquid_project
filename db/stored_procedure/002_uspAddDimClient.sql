USE [Liquid]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	  ariel.c.andrade
-- Create date: 24-03-2019
-- =============================================

IF OBJECT_ID ( 'dbo.uspAddDimClient', 'P' ) IS NOT NULL
  DROP PROCEDURE [dbo].[uspAddDimClient]
GO

CREATE PROCEDURE [dbo].[uspAddDimClient]
AS
BEGIN
  SET NOCOUNT ON;

  INSERT INTO [dbo].[dim_clients] ([client_name], [dim_project_id])
  SELECT DISTINCT [tt].[tmp_project_name], [dimp].[id]
  FROM [dbo].[tmp_fact_call_details] AS tt
  INNER JOIN [dbo].[dim_projects] AS dimp ON [tt].[tmp_parent_project] = [dimp].[project_name]
  LEFT JOIN [dbo].[dim_clients] AS dp ON [tt].[tmp_project_name] = [dp].[client_name]
  WHERE (([dp].[id] Is NULL) AND ([tt].[tmp_project_name] Is NOT NULL));
END
GO
