USE [Liquid]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[dim_languages]') AND type in (N'U'))
  DROP TABLE [dbo].[dim_languages]
  GO

USE [Liquid]
GO

CREATE TABLE [dbo].[dim_languages] (
  [id] INT NOT NULL IDENTITY(1, 1),
  [language_name] VARCHAR (50) NOT NULL DEFAULT '',
  [active] BIT NOT NULL DEFAULT 1,
  CONSTRAINT "PK_dim_languages" PRIMARY KEY ([id]),
  CONSTRAINT "index_dim_languages_on_language_name" UNIQUE ([language_name])
)
GO

INSERT INTO [dbo].[dim_languages] ([language_name]) VALUES ('undefined')
GO
