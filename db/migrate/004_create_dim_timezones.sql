USE [Liquid]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[dim_timezones]') AND type in (N'U'))
  DROP TABLE [dbo].[dim_timezones]
  GO

USE [Liquid]
GO

CREATE TABLE [dbo].[dim_timezones] (
  [id] INT NOT NULL IDENTITY(1, 1),
  [timezone_name] VARCHAR (100) NOT NULL DEFAULT '',
  [active] BIT NOT NULL DEFAULT 1,
  CONSTRAINT "PK_dim_timezones" PRIMARY KEY ([id]),
  CONSTRAINT "index_dim_timezones_on_timezone_name" UNIQUE ([timezone_name])
)
GO

INSERT INTO [dbo].[dim_timezones] ([timezone_name]) VALUES ('undefined')
GO
