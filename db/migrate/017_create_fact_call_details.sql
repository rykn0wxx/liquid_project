USE [Liquid]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fact_call_details]') AND type in (N'U'))
  DROP TABLE [dbo].[fact_call_details]
GO

USE [Liquid]
GO

CREATE TABLE [dbo].[fact_call_details] (
  [id] INT NOT NULL IDENTITY(1, 1),
  [dim_client_id] INT NOT NULL,
  [dim_timezone_id] INT NOT NULL,
  [dim_call_category_id] INT NOT NULL,
  [dim_call_type_id] INT NOT NULL,
  [dim_call_action_id] INT NOT NULL,
  [dim_call_action_reason_id] INT NOT NULL,
  [dim_call_disp_id] INT,
  [dim_call_service_id] INT NOT NULL,
  [dim_uip_user_id] INT,
  [dim_ivr_param1_id] INT,
  [dim_ivr_param2_id] INT,
  [dim_ivr_param3_id] INT,
  [dim_ivr_param4_id] INT,
  [dim_ivr_param9_id] INT,
  [dim_ivr_param10_id] INT,
  [dim_ivr_param16_id] INT,
  [dim_ivr_param18_id] INT,
  [dim_ivr_param19_id] INT,
  [call_id] INT NOT NULL,
  [call_seq] INT NOT NULL,
  [call_dialed_num] VARCHAR (100),
  [call_answer_date] DATETIME,
  [call_end_date] DATETIME NOT NULL,
  [call_end_date_tz] DATETIME NOT NULL,
  [call_start_date] DATETIME NOT NULL,
  [call_start_date_tz] DATETIME NOT NULL,
  [call_wrap_end_date] DATETIME,
  [call_hold_number] INT DEFAULT 0,
  [call_queue_time] INT DEFAULT 0,
  [call_talk_time] INT DEFAULT 0,
  [call_hold_time] INT DEFAULT 0,
  [call_wrap_time] INT DEFAULT 0,
  [created_at] DATETIME,
  CONSTRAINT "PK_fact_call_details" PRIMARY KEY ([id]),
  CONSTRAINT "FK_fact_call_details_dim_client_id"
    FOREIGN KEY ([dim_client_id])
    REFERENCES [dbo].[dim_clients] ([id]),
  CONSTRAINT "FK_fact_call_details_dim_timezone_id"
    FOREIGN KEY ([dim_timezone_id])
    REFERENCES [dbo].[dim_timezones] ([id]),
  CONSTRAINT "FK_fact_call_details_dim_call_category_id"
    FOREIGN KEY ([dim_call_category_id])
    REFERENCES [dbo].[dim_call_categories] ([id]),
  CONSTRAINT "FK_fact_call_details_dim_call_type_id"
    FOREIGN KEY ([dim_call_type_id])
    REFERENCES [dbo].[dim_call_types] ([id]),
  CONSTRAINT "FK_fact_call_details_dim_call_action_id"
    FOREIGN KEY ([dim_call_action_id])
    REFERENCES [dbo].[dim_call_actions] ([id]),
  CONSTRAINT "FK_fact_call_details_dim_call_action_reason_id"
    FOREIGN KEY ([dim_call_action_reason_id])
    REFERENCES [dbo].[dim_call_action_reasons] ([id]),
  CONSTRAINT "FK_fact_call_details_dim_call_disp_id"
    FOREIGN KEY ([dim_call_disp_id])
    REFERENCES [dbo].[dim_call_disps] ([id]),
  CONSTRAINT "FK_fact_call_details_dim_call_service_id"
    FOREIGN KEY ([dim_call_service_id])
    REFERENCES [dbo].[dim_call_services] ([id]),
  CONSTRAINT "FK_fact_call_details_dim_uip_user_id"
    FOREIGN KEY ([dim_uip_user_id])
    REFERENCES [dbo].[dim_uip_users] ([id]),
  CONSTRAINT "FK_fact_call_details_dim_ivr_param1_id"
    FOREIGN KEY ([dim_ivr_param1_id])
    REFERENCES [dbo].[dim_ivr_params] ([id]),
  CONSTRAINT "FK_fact_call_details_dim_ivr_param2_id"
    FOREIGN KEY ([dim_ivr_param2_id])
    REFERENCES [dbo].[dim_ivr_params] ([id]),
  CONSTRAINT "FK_fact_call_details_dim_ivr_param3_id"
    FOREIGN KEY ([dim_ivr_param3_id])
    REFERENCES [dbo].[dim_ivr_params] ([id]),
  CONSTRAINT "FK_fact_call_details_dim_ivr_param4_id"
    FOREIGN KEY ([dim_ivr_param4_id])
    REFERENCES [dbo].[dim_ivr_params] ([id]),
  CONSTRAINT "FK_fact_call_details_dim_ivr_param9_id"
    FOREIGN KEY ([dim_ivr_param9_id])
    REFERENCES [dbo].[dim_ivr_params] ([id]),
  CONSTRAINT "FK_fact_call_details_dim_ivr_param10_id"
    FOREIGN KEY ([dim_ivr_param10_id])
    REFERENCES [dbo].[dim_ivr_params] ([id]),
  CONSTRAINT "FK_fact_call_details_dim_ivr_param16_id"
    FOREIGN KEY ([dim_ivr_param16_id])
    REFERENCES [dbo].[dim_ivr_params] ([id]),
  CONSTRAINT "FK_fact_call_details_dim_ivr_param18_id"
    FOREIGN KEY ([dim_ivr_param18_id])
    REFERENCES [dbo].[dim_ivr_params] ([id]),
  CONSTRAINT "FK_fact_call_details_dim_ivr_param19_id"
    FOREIGN KEY ([dim_ivr_param19_id])
    REFERENCES [dbo].[dim_ivr_params] ([id])
)
GO
