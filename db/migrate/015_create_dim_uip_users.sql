USE [Liquid]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[dim_uip_users]') AND type in (N'U'))
  DROP TABLE [dbo].[dim_uip_users]
  GO

USE [Liquid]
GO

CREATE TABLE [dbo].[dim_uip_users] (
  [id] INT NOT NULL IDENTITY(1, 1),
  [uip_id] VARCHAR (50) NOT NULL DEFAULT '',
  [dim_emp_user_id] INT NOT NULL,
  [dim_project_id] INT NOT NULL,
  [start_date] DATE NOT NULL,
  [end_date] DATE,
  [supervisor_id] INT,
  [active] BIT NOT NULL DEFAULT 1,
  CONSTRAINT "PK_dim_uip_users" PRIMARY KEY ([id]),
  CONSTRAINT "index_dim_uip_users_on_uip_id" UNIQUE ([uip_id], [dim_emp_user_id]),
  CONSTRAINT "FK_dim_uip_users_dim_emp_user_id"
    FOREIGN KEY ([dim_emp_user_id])
    REFERENCES [dim_emp_users] ([id]),
  CONSTRAINT "FK_dim_uip_users_dim_project_id"
    FOREIGN KEY ([dim_project_id])
    REFERENCES [dim_projects] ([id])
)
GO

INSERT INTO [dbo].[dim_uip_users] ([uip_id], [dim_emp_user_id], [dim_project_id], [start_date])
VALUES ('undefined', 1, 1, '2000-01-01')
GO
