USE [Liquid]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[dim_uip_services]') AND type in (N'U'))
  DROP TABLE [dbo].[dim_uip_services]
  GO

USE [Liquid]
GO

CREATE TABLE [dbo].[dim_uip_services] (
  [id] INT NOT NULL IDENTITY(1, 1),
  [service_id] INT  NOT NULL,
  [dim_call_service_id] INT NOT NULL,
  [dim_language_id] INT NOT NULL,
  [dim_client_id] INT NOT NULL,
  [active] BIT NOT NULL DEFAULT 1,
  CONSTRAINT "PK_dim_uip_services" PRIMARY KEY ([id]),
  CONSTRAINT "index_dim_uip_services_on_service_id_dim_call_service_id_dim_language_id_dim_client_id" UNIQUE ([service_id], [dim_call_service_id], [dim_language_id], [dim_client_id]),
  CONSTRAINT "FK_dim_uip_services_dim_call_service_id"
    FOREIGN KEY ([dim_call_service_id])
    REFERENCES [dbo].[dim_call_services] ([id]),
  CONSTRAINT "FK_dim_uip_services_dim_language_id"
    FOREIGN KEY ([dim_language_id])
    REFERENCES [dbo].[dim_languages] ([id]),
  CONSTRAINT "FK_dim_uip_services_dim_client_id"
    FOREIGN KEY ([dim_client_id])
    REFERENCES [dbo].[dim_clients] ([id])
)
GO

INSERT INTO [dbo].[dim_uip_services] ([service_id], [dim_call_service_id], [dim_language_id], [dim_client_id])
VALUES (0, 1, 1, 1)
GO
