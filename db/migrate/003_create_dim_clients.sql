USE [Liquid]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[dim_clients]') AND type in (N'U'))
  DROP TABLE [dbo].[dim_clients]
  GO

USE [Liquid]
GO

CREATE TABLE [dbo].[dim_clients] (
  [id] INT NOT NULL IDENTITY(1, 1),
  [client_name] VARCHAR (100) NOT NULL DEFAULT '',
  [dim_project_id] INT NOT NULL,
  [active] BIT NOT NULL DEFAULT 1,
  CONSTRAINT "PK_dim_clients" PRIMARY KEY ([id]),
  CONSTRAINT "index_dim_clients_on_client_name" UNIQUE ([client_name]),
  CONSTRAINT "FK_dim_clients_dim_project_id"
    FOREIGN KEY ([dim_project_id])
    REFERENCES [dbo].[dim_projects] ([id])
)
GO

INSERT INTO [dbo].[dim_clients] ([client_name], [dim_project_id]) VALUES ('undefined', 1)
GO
