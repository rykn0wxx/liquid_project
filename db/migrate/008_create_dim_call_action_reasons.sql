USE [Liquid]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[dim_call_action_reasons]') AND type in (N'U'))
  DROP TABLE [dbo].[dim_call_action_reasons]
  GO

USE [Liquid]
GO

CREATE TABLE [dbo].[dim_call_action_reasons] (
  [id] INT NOT NULL IDENTITY(1, 1),
  [call_action_reason] VARCHAR (50) NOT NULL DEFAULT '',
  [active] BIT NOT NULL DEFAULT 1,
  CONSTRAINT "PK_dim_call_action_reasons" PRIMARY KEY ([id]),
  CONSTRAINT "index_dim_call_action_reasons_on_call_action_reason" UNIQUE ([call_action_reason])
)
GO

INSERT INTO [dbo].[dim_call_action_reasons] ([call_action_reason]) VALUES ('undefined')
GO
