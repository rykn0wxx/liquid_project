USE [Liquid]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[dim_call_services]') AND type in (N'U'))
  DROP TABLE [dbo].[dim_call_services]
  GO

USE [Liquid]
GO

CREATE TABLE [dbo].[dim_call_services] (
  [id] INT NOT NULL IDENTITY(1, 1),
  [call_service] VARCHAR (100) NOT NULL DEFAULT '',
  [active] BIT NOT NULL DEFAULT 1,
  CONSTRAINT "PK_dim_call_services" PRIMARY KEY ([id]),
  CONSTRAINT "index_dim_call_services_on_call_service" UNIQUE ([call_service])
)
GO

INSERT INTO [dbo].[dim_call_services] ([call_service]) VALUES ('undefined')
GO
