USE [Liquid]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[dim_call_actions]') AND type in (N'U'))
  DROP TABLE [dbo].[dim_call_actions]
  GO

USE [Liquid]
GO

CREATE TABLE [dbo].[dim_call_actions] (
  [id] INT NOT NULL IDENTITY(1, 1),
  [call_action] VARCHAR (50) NOT NULL DEFAULT '',
  [active] BIT NOT NULL DEFAULT 1,
  CONSTRAINT "PK_dim_call_actions" PRIMARY KEY ([id]),
  CONSTRAINT "index_dim_call_actions_on_call_action" UNIQUE ([call_action])
)
GO

INSERT INTO [dbo].[dim_call_actions] ([call_action]) VALUES ('undefined')
GO
