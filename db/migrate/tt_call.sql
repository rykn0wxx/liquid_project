USE [Liquid]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tt_call]') AND type in (N'U'))
  DROP TABLE [dbo].[tt_call]
  GO

USE [Liquid]
GO

CREATE TABLE [dbo].[tt_call] (
  [id] INT NOT NULL IDENTITY(1, 1),
  [tmp_parent_project] VARCHAR (100),
  [tmp_project_name] VARCHAR (100),
  [tmp_timezone] VARCHAR (100),
  [tmp_call_category] VARCHAR (100),
  [tmp_call_type] VARCHAR (100),
  [tmp_call_action] VARCHAR (100),
  [tmp_call_action_reason] VARCHAR (100),
  [tmp_disp] VARCHAR (100),
  [tmp_service_id] INT,
  [tmp_service_name] VARCHAR (100),
  [tmp_orig_srv] INT,
  [tmp_user_id] VARCHAR (100),
  [tmp_ivr_param_1] VARCHAR (100),
  [tmp_ivr_param_2] VARCHAR (100),
  [tmp_ivr_param_3] VARCHAR (100),
  [tmp_ivr_param_4] VARCHAR (100),
  [tmp_ivr_param_9] VARCHAR (100),
  [tmp_ivr_param_10] VARCHAR (100),
  [tmp_ivr_param_16] VARCHAR (100),
  [tmp_ivr_param_18] VARCHAR (100),
  [tmp_ivr_param_19] VARCHAR (100),
  [tmp_call] INT,
  [tmp_seq] INT,
  [tmp_dialed_num] VARCHAR (100),
  [tmp_answer_date] DATETIME,
  [tmp_end_date] DATETIME,
  [tmp_end_date_tz] DATETIME,
  [tmp_start_date] DATETIME,
  [tmp_start_date_tz] DATETIME,
  [tmp_wrap_end_date] DATETIME,
  [tmp_hold_number] INT,
  [tmp_queue_time] INT,
  [tmp_talk_time] INT,
  [tmp_hold_time] INT,
  [tmp_wrap_time] INT,
  CONSTRAINT "PK_tt_call" PRIMARY KEY ([id])
)
GO
