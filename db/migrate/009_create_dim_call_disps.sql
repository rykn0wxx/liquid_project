USE [Liquid]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[dim_call_disps]') AND type in (N'U'))
  DROP TABLE [dbo].[dim_call_disps]
  GO

USE [Liquid]
GO

CREATE TABLE [dbo].[dim_call_disps] (
  [id] INT NOT NULL IDENTITY(1, 1),
  [call_disp] VARCHAR (100) NOT NULL DEFAULT '',
  [active] BIT NOT NULL DEFAULT 1,
  CONSTRAINT "PK_dim_call_disps" PRIMARY KEY ([id]),
  CONSTRAINT "index_dim_call_disps_on_call_disp" UNIQUE ([call_disp])
)
GO

INSERT INTO [dbo].[dim_call_disps] ([call_disp]) VALUES ('undefined')
GO
