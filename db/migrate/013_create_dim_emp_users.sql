USE [Liquid]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[dim_emp_users]') AND type in (N'U'))
  DROP TABLE [dbo].[dim_emp_users]
  GO

USE [Liquid]
GO

CREATE TABLE [dbo].[dim_emp_users] (
  [id] INT NOT NULL IDENTITY(1, 1),
  [emp_id] VARCHAR (50) NOT NULL DEFAULT '',
  [login_id] VARCHAR (50) NOT NULL DEFAULT '',
  [first_name] VARCHAR (100) NOT NULL DEFAULT '',
  [last_name] VARCHAR (100) NOT NULL DEFAULT '',
  [hire_date] DATE NOT NULL,
  [term_date] DATE,
  [supervisor_id] INT,
  [active] BIT NOT NULL DEFAULT 1,
  CONSTRAINT "PK_dim_emp_users" PRIMARY KEY ([id]),
  CONSTRAINT "index_dim_emp_users_on_emp_id_login_id" UNIQUE ([emp_id], [login_id])
)
GO

INSERT INTO [dbo].[dim_emp_users] ([emp_id], [login_id], [first_name], [last_name], [hire_date])
VALUES ('0000', 'undefined', 'undefined', 'undefined', '2000-01-01')
GO
