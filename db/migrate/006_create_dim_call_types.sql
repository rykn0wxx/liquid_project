USE [Liquid]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[dim_call_types]') AND type in (N'U'))
  DROP TABLE [dbo].[dim_call_types]
  GO

USE [Liquid]
GO

CREATE TABLE [dbo].[dim_call_types] (
  [id] INT NOT NULL IDENTITY(1, 1),
  [call_type] VARCHAR (20) NOT NULL DEFAULT '',
  [active] BIT NOT NULL DEFAULT 1,
  CONSTRAINT "PK_dim_call_types" PRIMARY KEY ([id]),
  CONSTRAINT "index_dim_call_types_on_call_type" UNIQUE ([call_type])
)
GO

INSERT INTO [dbo].[dim_call_types] ([call_type]) VALUES ('undefined')
GO
