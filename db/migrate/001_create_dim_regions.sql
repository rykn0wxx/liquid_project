USE [Liquid]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[dim_regions]') AND type in (N'U'))
  DROP TABLE [dbo].[dim_regions]
  GO

USE [Liquid]
GO

CREATE TABLE [dbo].[dim_regions] (
  [id] INT NOT NULL IDENTITY(1, 1),
  [region_name] VARCHAR (50) NOT NULL DEFAULT '',
  [active] BIT NOT NULL DEFAULT 1,
  CONSTRAINT "PK_dim_regions" PRIMARY KEY ([id]),
  CONSTRAINT "index_dim_regions_on_region_name" UNIQUE ([region_name])
)
GO

INSERT INTO [dbo].[dim_regions] ([region_name]) VALUES ('undefined')
GO
