USE [Liquid]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[dim_ivr_params]') AND type in (N'U'))
  DROP TABLE [dbo].[dim_ivr_params]
  GO

USE [Liquid]
GO

CREATE TABLE [dbo].[dim_ivr_params] (
  [id] INT NOT NULL IDENTITY(1, 1),
  [ivr_param] VARCHAR (100) NOT NULL DEFAULT '',
  [ivr_code] INT NOT NULL,
  [dim_project_id] INT NOT NULL,
  [active] BIT NOT NULL DEFAULT 1,
  CONSTRAINT "PK_dim_ivr_params" PRIMARY KEY ([id]),
  CONSTRAINT "index_dim_ivr_params_on_ivr_param_ivr_code_dim_project_id" UNIQUE ([ivr_param], [ivr_code], [dim_project_id]),
  CONSTRAINT "FK_dim_ivr_params_dim_project_id"
    FOREIGN KEY ([dim_project_id])
    REFERENCES [dbo].[dim_projects] ([id])
)
GO

INSERT INTO [dbo].[dim_ivr_params] ([ivr_param], [ivr_code], [dim_project_id]) VALUES ('undefined', 0, 1)
GO
