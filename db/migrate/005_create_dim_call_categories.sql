USE [Liquid]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[dim_call_categories]') AND type in (N'U'))
  DROP TABLE [dbo].[dim_call_categories]
  GO

USE [Liquid]
GO

CREATE TABLE [dbo].[dim_call_categories] (
  [id] INT NOT NULL IDENTITY(1, 1),
  [call_category] VARCHAR (20) NOT NULL DEFAULT '',
  [active] BIT NOT NULL DEFAULT 1,
  CONSTRAINT "PK_dim_call_categories" PRIMARY KEY ([id]),
  CONSTRAINT "index_dim_call_categories_on_call_category" UNIQUE ([call_category])
)
GO

INSERT INTO [dbo].[dim_call_categories] ([call_category]) VALUES ('undefined')
GO
