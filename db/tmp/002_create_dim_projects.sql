USE [Liquid]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tmp_dim_projects]') AND type in (N'U'))
  DROP TABLE [dbo].[tmp_dim_projects]
  GO

USE [Liquid]
GO

CREATE TABLE [dbo].[tmp_dim_projects] (
  [id] INT NOT NULL IDENTITY(1, 1),
  [project_name] VARCHAR (100) NOT NULL DEFAULT '',
  [active] BIT NOT NULL DEFAULT 1,
  CONSTRAINT "PK_tmp_dim_projects" PRIMARY KEY ([id]),
  CONSTRAINT "index_tmp_dim_projects_on_project_name" UNIQUE ([project_name])
)
GO

INSERT INTO [dbo].[tmp_dim_projects] ([project_name]) VALUES ('undefined')
GO
