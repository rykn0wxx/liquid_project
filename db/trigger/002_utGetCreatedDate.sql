USE [Liquid]
GO

-- =============================================
-- Author:		ariel.c.andrade
-- Create date: 24-03-2019
-- Description:	Update created_at
-- =============================================

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[UpdateCreatedAt]'))
  DROP TRIGGER [dbo].[UpdateCreatedAt]
GO

USE [Liquid]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[UpdateCreatedAt]
  ON [dbo].[fact_call_details]
  AFTER INSERT

AS

BEGIN
  SET NOCOUNT ON;
  DECLARE @getDate DATETIME = GETDATE()

  UPDATE
    [dbo].[fact_call_details]
  SET
    [created_at] = @getDate
  FROM
    [dbo].[fact_call_details] AS f
    INNER JOIN INSERTED AS i ON ([f].[id] = [i].[id])
    LEFT OUTER JOIN DELETED AS d ON ([i].[id] = [d].[id])
  WHERE
    [d].[id] Is Null

END
GO
