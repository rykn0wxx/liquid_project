USE [Liquid]
GO

-- =============================================
-- Author:		ariel.c.andrade
-- Create date: 24-03-2019
-- Description:	After insert into tmp_fact
-- =============================================

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[UpdateDimensions]'))
  DROP TRIGGER [dbo].[UpdateDimensions]
GO

USE [Liquid]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[UpdateDimensions]
  ON [dbo].[tmp_fact_call_details]
  AFTER INSERT

AS

BEGIN
  SET NOCOUNT ON;
  EXEC [dbo].[uspAddDimProject]
  EXEC [dbo].[uspAddDimClient]
  EXEC [dbo].[uspAddDimTimezone]
  EXEC [dbo].[uspAddDimCallCategory]
  EXEC [dbo].[uspAddDimCallType]
  EXEC [dbo].[uspAddDimCallAction]
  EXEC [dbo].[uspAddDimCallActionReason]
  EXEC [dbo].[uspAddDimCallDisp]
  EXEC [dbo].[uspAddDimCallService]
  EXEC [dbo].[uspAddDimIvrParam1]
  EXEC [dbo].[uspAddDimIvrParam2]
  EXEC [dbo].[uspAddDimIvrParam3]
  EXEC [dbo].[uspAddDimIvrParam4]
  EXEC [dbo].[uspAddDimIvrParam9]
  EXEC [dbo].[uspAddDimIvrParam10]
  EXEC [dbo].[uspAddDimIvrParam16]
  EXEC [dbo].[uspAddDimIvrParam18]
END
GO
