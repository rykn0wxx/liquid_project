def table_generator(tbl_name, fld_name, fld_size = 100)
<<SQLTBL
USE [Liquid]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[#{ tbl_name }]') AND type in (N'U'))
  DROP TABLE [dbo].[#{ tbl_name }]
  GO

USE [Liquid]
GO

CREATE TABLE [dbo].[#{ tbl_name }] (
  [id] INT NOT NULL IDENTITY(1, 1),
  [#{ fld_name }] VARCHAR (#{ fld_size }) NOT NULL DEFAULT '',
  [active] BIT NOT NULL DEFAULT 1,
  CONSTRAINT "PK_#{ tbl_name }" PRIMARY KEY ([id]),
  CONSTRAINT "index_#{ tbl_name }_on_#{ fld_name }" UNIQUE ([#{ fld_name }])
)
GO
SQLTBL
end

def create_file_name fle_name
  base_path = 'db/migrate'
  dir_files = Dir.entries(File.expand_path(base_path)).select { |f| !File.directory? f }.size + 1
  num_files = dir_files.to_s
  if num_files.length == 2
    num_files = '0' + num_files
  else
    num_files = '00' + num_files
  end
  out_file = File.join(base_path, num_files + '_create_' + fle_name + '.sql')
  out_file
end

def create_migration(fle_config)
  tbl_name = fle_config[:tbl_name]
  fld_name = fle_config[:fld_name]
  gen_sql = table_generator(tbl_name, fld_name, fle_config[:fld_size])
  sql_path = File.expand_path(create_file_name(tbl_name))
  sql_file = File.new(sql_path, 'w')
  sql_file.puts(gen_sql)
  sql_file.close
  # File.open(sql_path, 'w') { |file| file.write(gen_sql) }
end

ARR_FILES = [
  {
    tbl_name: 'dim_regions',
    fld_name: 'region_name',
    fld_size: 50
  },
  {
    tbl_name: 'dim_projects',
    fld_name: 'project_name',
    fld_size: 100
  },
  {
    tbl_name: 'dim_clients',
    fld_name: 'client_name',
    fld_size: 100
  },
  {
    tbl_name: 'dim_timezones',
    fld_name: 'timezone_name',
    fld_size: 100
  },
  {
    tbl_name: 'dim_call_categories',
    fld_name: 'call_category',
    fld_size: 20
  },
  {
    tbl_name: 'dim_call_types',
    fld_name: 'call_type',
    fld_size: 20
  },
  {
    tbl_name: 'dim_call_actions',
    fld_name: 'call_action',
    fld_size: 50
  },
  {
    tbl_name: 'dim_call_action_reasons',
    fld_name: 'call_action_reason',
    fld_size: 50
  },
  {
    tbl_name: 'dim_call_disps',
    fld_name: 'call_disp',
    fld_size: 100
  },
  {
    tbl_name: 'dim_languages',
    fld_name: 'language_name',
    fld_size: 50
  },
  {
    tbl_name: 'dim_call_services',
    fld_name: 'call_service',
    fld_size: 100
  },
  {
    tbl_name: 'dim_ivr_params',
    fld_name: 'ivr_param',
    fld_size: 100
  },
  {
    tbl_name: 'dim_emp_users',
    fld_name: 'emp_id',
    fld_size: 50
  },
  {
    tbl_name: 'dim_uip_services',
    fld_name: 'service_id',
    fld_size: 100
  },
  {
    tbl_name: 'dim_uip_users',
    fld_name: 'uip_id',
    fld_size: 10
  }
]

ARR_FILES.each do |arr_file|
  create_migration arr_file
end

