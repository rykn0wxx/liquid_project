#!/usr/bin/env ruby.exe

class StoredProcCreator
  def initialize(src_tbl, src_fld, dst_tbl, dst_fld)
    @src_tbl = src_tbl
    @src_fld = src_fld
    @dst_tbl = dst_tbl
    @dst_fld = dst_fld
    @dst_tbls = dst_tbl
    @proc_name = create_proc_name(dst_tbl)
  end
  def generate_sp_file
    sp_path = File.expand_path(create_file_name)
    sp_file = File.new(sp_path, 'w')
    sp_file.puts(create_sp)
    sp_file.close
  end
  private
  def create_file_name
    # base_path = 'db/stored_procedure'
    base_path = '../tickets/stored_procedure'
    dir_files = Dir.entries(File.expand_path(base_path)).select { |f| !File.directory? f }.size + 1
    num_files = dir_files.to_s
    if num_files.length == 2
      num_files = '0' + num_files
    else
      num_files = '00' + num_files
    end
    out_file = File.join(base_path, num_files + '_' + @proc_name + '.sql')
    out_file
  end
  def create_sp
<<SQLSP
USE [Liquid]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	  ariel.c.andrade
-- Create date: 11-04-2019
-- Description:	Add new dimension
-- =============================================

IF OBJECT_ID ( 'dbo.#{ @proc_name }', 'P' ) IS NOT NULL
  DROP PROCEDURE [dbo].[#{ @proc_name }]
GO

CREATE PROCEDURE [dbo].[#{ @proc_name }]
AS
BEGIN
  SET NOCOUNT ON;

  INSERT INTO [dbo].[#{ @dst_tbls }] ([#{ @dst_fld }])
  SELECT DISTINCT [tt].[#{ @src_fld }]
  FROM [dbo].[#{ @src_tbl }] AS tt
  LEFT JOIN [dbo].[#{ @dst_tbls }] AS dp ON [tt].[#{ @src_fld }] = [dp].[#{ @dst_fld }]
  WHERE (([dp].[id] Is NULL) AND ([tt].[#{ @src_fld }] Is NOT NULL));
END
GO
SQLSP
  end
  def create_proc_name(nme)
    tmp_name = nme.split('_')
    prc_name = 'uspAdd' + tmp_name.map { |x| x.capitalize }.join
    prc_name
  end
end

TBL_ARR = [
  {
    src_tbl: 'tmp_fact_tickets',
    src_fld: 'dim_ticket_state_id',
    dst_tbl: 'dim_ticket_states',
    dst_fld: 'ticket_state'
  },
  {
    src_tbl: 'tmp_fact_tickets',
    src_fld: 'dim_ticket_type_id',
    dst_tbl: 'dim_ticket_types',
    dst_fld: 'ticket_type'
  },
  {
    src_tbl: 'tmp_fact_tickets',
    src_fld: 'dim_ticket_contact_id',
    dst_tbl: 'dim_ticket_contacts',
    dst_fld: 'ticket_contact'
  },
  {
    src_tbl: 'tmp_fact_tickets',
    src_fld: 'dim_ticket_priority_id',
    dst_tbl: 'dim_ticket_priorities',
    dst_fld: 'ticket_priority'
  },
  {
    src_tbl: 'tmp_fact_tickets',
    src_fld: 'dim_ticket_urgency_id',
    dst_tbl: 'dim_ticket_urgencies',
    dst_fld: 'ticket_urgency'
  },
  {
    src_tbl: 'tmp_fact_tickets',
    src_fld: 'dim_ticket_impact_id',
    dst_tbl: 'dim_ticket_impacts',
    dst_fld: 'ticket_impact'
  },
  {
    src_tbl: 'tmp_fact_tickets',
    src_fld: 'dim_ticket_category_id',
    dst_tbl: 'dim_ticket_categories',
    dst_fld: 'ticket_category'
  },
  {
    src_tbl: 'tmp_fact_tickets',
    src_fld: 'dim_ticket_subcategory_id',
    dst_tbl: 'dim_ticket_subcategories',
    dst_fld: 'ticket_subcategory'
  },
  {
    src_tbl: 'tmp_fact_tickets',
    src_fld: 'dim_business_service_id',
    dst_tbl: 'dim_business_services',
    dst_fld: 'business_service'
  },
  {
    src_tbl: 'tmp_fact_tickets',
    src_fld: 'dim_configuration_item_id',
    dst_tbl: 'dim_configuration_items',
    dst_fld: 'configuration_item'
  },
  {
    src_tbl: 'tmp_fact_tickets',
    src_fld: 'dim_resolution_code_id',
    dst_tbl: 'dim_resolution_codes',
    dst_fld: 'resolution_code'
  },
  {
    src_tbl: 'tmp_fact_tickets',
    src_fld: 'dim_resolution_method_id',
    dst_tbl: 'dim_resolution_methods',
    dst_fld: 'resolution_method'
  },
  {
    src_tbl: 'tmp_fact_tickets',
    src_fld: 'assignment_group_id',
    dst_tbl: 'dim_groups',
    dst_fld: 'group_name'
  }
]

FLE_ARR = [
  {
    src_tbl: 'tmp_fact_call_details',
    src_fld: 'tmp_parent_project',
    dst_tbl: 'dim_project',
    dst_fld: 'project_name'
  },
  {
    src_tbl: 'tmp_fact_call_details',
    src_fld: 'tmp_project_name',
    dst_tbl: 'dim_client',
    dst_fld: 'client_name'
  },
  {
    src_tbl: 'tmp_fact_call_details',
    src_fld: 'tmp_timezone',
    dst_tbl: 'dim_timezone',
    dst_fld: 'timezone_name'
  },
  {
    src_tbl: 'tmp_fact_call_details',
    src_fld: 'tmp_call_category',
    dst_tbl: 'dim_call_category',
    dst_fld: 'call_category'
  },
  {
    src_tbl: 'tmp_fact_call_details',
    src_fld: 'tmp_call_type',
    dst_tbl: 'dim_call_type',
    dst_fld: 'call_type'
  },
  {
    src_tbl: 'tmp_fact_call_details',
    src_fld: 'tmp_call_action',
    dst_tbl: 'dim_call_action',
    dst_fld: 'call_action'
  },
  {
    src_tbl: 'tmp_fact_call_details',
    src_fld: 'tmp_call_action_reason',
    dst_tbl: 'dim_call_action_reason',
    dst_fld: 'call_action_reason'
  },
  {
    src_tbl: 'tmp_fact_call_details',
    src_fld: 'tmp_disp',
    dst_tbl: 'dim_call_disp',
    dst_fld: 'call_disp'
  },
  {
    src_tbl: 'tmp_fact_call_details',
    src_fld: 'tmp_service_name',
    dst_tbl: 'dim_call_service',
    dst_fld: 'call_service'
  },
  {
    src_tbl: 'tmp_fact_call_details',
    src_fld: 'tmp_ivr_param_1',
    dst_tbl: 'dim_ivr_param',
    dst_fld: 'ivr_param'
  },
  {
    src_tbl: 'tmp_fact_call_details',
    src_fld: 'tmp_ivr_param_2',
    dst_tbl: 'dim_ivr_param',
    dst_fld: 'ivr_param'
  },
  {
    src_tbl: 'tmp_fact_call_details',
    src_fld: 'tmp_ivr_param_3',
    dst_tbl: 'dim_ivr_param',
    dst_fld: 'ivr_param'
  },
  {
    src_tbl: 'tmp_fact_call_details',
    src_fld: 'tmp_ivr_param_4',
    dst_tbl: 'dim_ivr_param',
    dst_fld: 'ivr_param'
  },
  {
    src_tbl: 'tmp_fact_call_details',
    src_fld: 'tmp_ivr_param_9',
    dst_tbl: 'dim_ivr_param',
    dst_fld: 'ivr_param'
  },
  {
    src_tbl: 'tmp_fact_call_details',
    src_fld: 'tmp_ivr_param_10',
    dst_tbl: 'dim_ivr_param',
    dst_fld: 'ivr_param'
  },
  {
    src_tbl: 'tmp_fact_call_details',
    src_fld: 'tmp_ivr_param_16',
    dst_tbl: 'dim_ivr_param',
    dst_fld: 'ivr_param'
  },
  {
    src_tbl: 'tmp_fact_call_details',
    src_fld: 'tmp_ivr_param_18',
    dst_tbl: 'dim_ivr_param',
    dst_fld: 'ivr_param'
  }
]

# FLE_ARR.each do |f|
#   a = StoredProcCreator.new(f[:src_tbl], f[:src_fld], f[:dst_tbl], f[:dst_fld])
#   a.generate_sp_file
# end

TBL_ARR.each do |f|
  a = StoredProcCreator.new(f[:src_tbl], f[:src_fld], f[:dst_tbl], f[:dst_fld])
  a.generate_sp_file
end
