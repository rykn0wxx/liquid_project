const fs = require('fs')
const path = require('path')

const targetPath = '/mud/codes/liquid_project/tickets/stored_procedure'
const fileReadOpts = {
  encoding: 'utf-8',
  flag: 'r'
}

function readFileContent(filePath) {
  const fileLoc = `${targetPath}/${filePath}`
  const buffer = fs.readFileSync(fileLoc, fileReadOpts)
  console.log(buffer)
}

function getAllFiles(targetPath) {
  fs.readdir(targetPath, (err, files) => {
    if (err) {
      console.error('Could not list the directory', err)
      process.exit(1)
    }
    files.forEach((file, index) => {
      readFileContent(file)
    })
  })
}

getAllFiles(targetPath)
