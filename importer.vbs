Option Explicit

Dim dbase_name, server_name, server_user, user_pw, rec_aff
Dim wshShell
Set wshShell = CreateObject( "WScript.Shell" )

Public Sub run_stored_proc(sp_name)
  Dim cnn, cnn_str
  cnn_str = "Provider=SQLOLEDB;Initial Catalog=" & dbase_name & ";Data Source=" & server_name & ";Uid=" & server_user & ";Pwd=" & user_pw
  Set cnn = CreateObject("ADODB.Connection")
  cnn.ConnectionString = cnn_str
  cnn.Open
  On Error Resume Next
  cnn.BeginTrans
  cnn.Execute "EXEC [dbo].[" & sp_name & "]"
  If Err.Number <> 0 Then
    cnn.RollbackTrans
    Err.Clear
  Else
    cnn.CommitTrans
  End If
  On Error Goto 0

  cnn.Close
  Set cnn = Nothing
End Sub

Public Sub upload_tmp_data(wb_fullname, ws_name)
  Dim cn, exl_con_str, sql_con_str, sql_str, job_start, job_result

  run_stored_proc "uspDeleteTmpFactCall"
  run_stored_proc "uspResetTmpFactIdentity"
  run_stored_proc "uspDropImportTbl"

  exl_con_str = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & wb_fullname & ";Extended Properties=Excel 12.0"
  sql_con_str = "[odbc;Driver={SQL Server};Server=" & server_name & ";Database=" & dbase_name & ";UID=" & server_user & ";PWD=" & user_pw & "].import_tbl"

  job_start = "Upload Start: " & FormatDateTime(Now())
  Set cn = CreateObject("ADODB.Connection")
  cn.ConnectionString = exl_con_str

  sql_str = "SELECT * INTO " & sql_con_str & " FROM [" & ws_name & "$]"

  cn.Open
  On Error Resume Next
  cn.BeginTrans
  cn.Execute sql_str, rec_aff, 128
  If Err.Number <> 0 Then
    cn.RollbackTrans
    WScript.Echo Err.Number
    Err.Clear
    WScript.Echo Err.Number
  Else
    cn.CommitTrans
    job_result = "Record(s) affected : " & rec_aff
  End If
  On Error GoTo 0

  WScript.Echo job_result & Chr(10) & job_start & Chr(10) & "Upload End: " & FormatDateTime(Now())
  cn.Close
  Set cn = Nothing
End Sub

Public Sub init_app()
  Dim wb, sh_name, fb_wb, fb_sh
  server_name = wshShell.ExpandEnvironmentStrings("%sql_server_name%")
  dbase_name = wshShell.ExpandEnvironmentStrings("%sql_server_catalog%")
  server_user = wshShell.ExpandEnvironmentStrings("%sql_server_login%")
  user_pw = wshShell.ExpandEnvironmentStrings("%sql_server_word%")
  fb_sh = "tmp_fact_call_details"
  fb_wb = "C:\mud\msys\home\aandrade\ofc_docs\global\telephony_worker.xlsx"
  wb = InputBox("Enter the complete file path of your raw data", "Data to be imported", fb_wb)
  If wb <> vbNullString Then
    sh_name = InputBox("Enter the name of the worksheet", "Sheet name", fb_sh)
    If sh_name <> vbNullString Then
      upload_tmp_data wb, sh_name
    End If
  End If

End Sub

init_app

Set wshShell = Nothing

WScript.Quit
